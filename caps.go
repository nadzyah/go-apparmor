// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package apparmor

import (
	"gitlab.com/zygoon/go-apparmor/internal"
)

// Capability identifies a specific Linux capability.
//
// Capabilities are permissions that a process may posses at runtime. Typically processes
// of the root user have all the capabilities. As a security measure processes drop capabilities
// that are not required for operation, trying to minimize the effective permissions that could
// be exploited by an attacker.
type CapabilityNumber uint32

// String returns the human-readable name of the capability.
func (c CapabilityNumber) String() string {
	return capabilityNumberValues.String(c)
}

// GoString returns the corresponding Go constant or literal.
func (c CapabilityNumber) GoString() string {
	return capabilityNumberValues.GoString(c)
}

// KernelString returns the corresponding Linux kernel constant or literal.
func (c CapabilityNumber) KernelString() string {
	return capabilityNumberValues.KernelString(c)
}

// ParserString returns the corresponding AppArmor parser constant or literal.
func (c CapabilityNumber) ParserString() string {
	return capabilityNumberValues.ParserString(c)
}

// Mask returns the corresponding bit-mask compatible with CapabilitySet.
func (c CapabilityNumber) Mask() CapabilitySet {
	return CapabilitySet(1 << c)
}

// CapabilitySet is a bit-set encoding Linux capabilities.
type CapabilitySet uint64

// String returns the human-readable name of the set of capabilities.
func (c CapabilitySet) String() string {
	return capabilitySetValues.String(c)
}

// GoString returns the corresponding Go expression.
func (c CapabilitySet) GoString() string {
	return capabilitySetValues.GoString(c)
}

// KernelString returns the corresponding Linux kernel constant or literal.
func (c CapabilitySet) KernelString() string {
	return capabilitySetValues.KernelString(c)
}

// ParserString returns the corresponding AppArmor parser constant or literal.
func (c CapabilitySet) ParserString() string {
	return capabilitySetValues.ParserString(c)
}

// Known values for Capability, as of Linux 6.8. The exact semantics is non-trivial.
// It is best to read capabilities(7) man page and cross-reference the Linux kernel source code.
const (
	CapabilityChangeOwner                          CapabilityNumber = 0
	CapabilityDiscretionaryAccessControlOverride   CapabilityNumber = 1
	CapabilityDiscretionaryAccessControlReadSearch CapabilityNumber = 2
	CapabilityFileOwner                            CapabilityNumber = 3
	CapabilitySetFileId                            CapabilityNumber = 4
	CapabilityKill                                 CapabilityNumber = 5
	CapabilitySetGroupId                           CapabilityNumber = 6
	CapabilitySetUserId                            CapabilityNumber = 7
	CapabilitySetProcessCapabilities               CapabilityNumber = 8
	CapabilityLinuxImmutable                       CapabilityNumber = 9
	CapabilityNetworkBindService                   CapabilityNumber = 10
	CapabilityNetworkBroadcast                     CapabilityNumber = 11
	CapabilityNetworkAdmin                         CapabilityNumber = 12
	CapabilityNetworkRaw                           CapabilityNumber = 13
	CapabilityIpcLock                              CapabilityNumber = 14
	CapabilityIpcOwner                             CapabilityNumber = 15
	CapabilitySystemModule                         CapabilityNumber = 16
	CapabilitySystemRawIo                          CapabilityNumber = 17
	CapabilitySystemChangeRoot                     CapabilityNumber = 18
	CapabilitySystemProcessTrace                   CapabilityNumber = 19
	CapabilitySystemProcessAccounting              CapabilityNumber = 20
	CapabilitySystemAdmin                          CapabilityNumber = 21
	CapabilitySystemBoot                           CapabilityNumber = 22
	CapabilitySystemNice                           CapabilityNumber = 23
	CapabilitySystemResource                       CapabilityNumber = 24
	CapabilitySystemTime                           CapabilityNumber = 25
	CapabilitySystemTerminalConfig                 CapabilityNumber = 26
	CapabilityMakeNode                             CapabilityNumber = 27
	CapabilityLease                                CapabilityNumber = 28
	CapabilityAuditWrite                           CapabilityNumber = 29
	CapabilityAuditControl                         CapabilityNumber = 30
	CapabilitySetFileCapabilities                  CapabilityNumber = 31
	CapabilityMandatoryAccessControlOverride       CapabilityNumber = 32
	CapabilityMandatoryAccessControlAdmin          CapabilityNumber = 33
	CapabilitySystemLog                            CapabilityNumber = 34
	CapabilityWakeAlarm                            CapabilityNumber = 35
	CapabilityBlockSuspend                         CapabilityNumber = 36
	CapabilityAuditRead                            CapabilityNumber = 37
	CapabilityPerformanceMonitor                   CapabilityNumber = 38
	CapabilityBPF                                  CapabilityNumber = 39
	CapabilityCheckpointRestore                    CapabilityNumber = 40
)

var capabilityNumberValues = internal.EnumValues[CapabilityNumber]{
	{
		Value:      CapabilityChangeOwner,
		Name:       "chown",
		GoName:     "CapabilityChangeOwner",
		KernelName: "CAP_CHOWN",
		ParserName: "CAP_CHOWN",
	},
	{
		Value:      CapabilityDiscretionaryAccessControlOverride,
		Name:       "dac-override",
		GoName:     "CapabilityDiscretionaryAccessControlOverride",
		KernelName: "CAP_DAC_OVERRIDE",
		ParserName: "CAP_DAC_OVERRIDE",
	},
	{
		Value:      CapabilityDiscretionaryAccessControlReadSearch,
		Name:       "dac-read-search",
		GoName:     "CapabilityDiscretionaryAccessControlReadSearch",
		KernelName: "CAP_DAC_READ_SEARCH",
		ParserName: "CAP_DAC_READ_SEARCH",
	},
	{
		Value:      CapabilityFileOwner,
		Name:       "fowner",
		GoName:     "CapabilityFileOwner",
		KernelName: "CAP_FOWNER",
		ParserName: "CAP_FOWNER",
	},
	{
		Value:      CapabilitySetFileId,
		Name:       "fsetid",
		GoName:     "CapabilitySetFileID",
		KernelName: "CAP_FSETID",
		ParserName: "CAP_FSETID",
	},
	{
		Value:      CapabilityKill,
		Name:       "kill",
		GoName:     "CapabilityKill",
		KernelName: "CAP_KILL",
		ParserName: "CAP_KILL",
	},
	{
		Value:      CapabilitySetGroupId,
		Name:       "setgid",
		GoName:     "CapabilitySetGroupId",
		KernelName: "CAP_SETGID",
		ParserName: "CAP_SETGID",
	},
	{
		Value:      CapabilitySetUserId,
		Name:       "setuid",
		GoName:     "CapabilitySetUserId",
		KernelName: "CAP_SETUID",
		ParserName: "CAP_SETUID",
	},
	{
		Value:      CapabilitySetProcessCapabilities,
		Name:       "setpcap",
		GoName:     "CapabilitySetProcessCapabilities",
		KernelName: "CAP_SETPCAP",
		ParserName: "CAP_SETPCAP",
	},
	{
		Value:      CapabilityLinuxImmutable,
		Name:       "linux-immutable",
		GoName:     "CapabilityLinuxImmutable",
		KernelName: "CAP_LINUX_IMMUTABLE",
		ParserName: "CAP_LINUX_IMMUTABLE",
	},
	{
		Value:      CapabilityNetworkBindService,
		Name:       "net-bind-service",
		GoName:     "CapabilityNetBindService",
		KernelName: "CAP_NET_BIND_SERVICE",
		ParserName: "CAP_NET_BIND_SERVICE",
	},
	{
		Value:      CapabilityNetworkBroadcast,
		Name:       "net-broadcast",
		GoName:     "CapabilityNetworkBroadcast",
		KernelName: "CAP_NET_BROADCAST",
		ParserName: "CAP_NET_BROADCAST",
	},
	{
		Value:      CapabilityNetworkAdmin,
		Name:       "net-admin",
		GoName:     "CapabilityNetworkAdmin",
		KernelName: "CAP_NET_ADMIN",
		ParserName: "CAP_NET_ADMIN",
	},
	{
		Value:      CapabilityNetworkRaw,
		Name:       "net-raw",
		GoName:     "CapabilityNetworkRaw",
		KernelName: "CAP_NET_RAW",
		ParserName: "CAP_NET_RAW",
	},
	{
		Value:      CapabilityIpcLock,
		Name:       "ipc-lock",
		GoName:     "CapabilityIpcLock",
		KernelName: "CAP_IPC_LOCK",
		ParserName: "CAP_IPC_LOCK",
	},
	{
		Value:      CapabilityIpcOwner,
		Name:       "ipc-owner",
		GoName:     "CapabilityIpcOwner",
		KernelName: "CAP_IPC_OWNER",
		ParserName: "CAP_IPC_OWNER",
	},
	{
		Value:      CapabilitySystemModule,
		Name:       "sys-module",
		GoName:     "CapabilitySysModule",
		KernelName: "CAP_SYS_MODULE",
		ParserName: "CAP_SYS_MODULE",
	},
	{
		Value:      CapabilitySystemRawIo,
		Name:       "sys-rawio",
		GoName:     "CapabilitySystemRawIo",
		KernelName: "CAP_SYS_RAWIO",
		ParserName: "CAP_SYS_RAWIO",
	},
	{
		Value:      CapabilitySystemChangeRoot,
		Name:       "sys-chroot",
		GoName:     "CapabilitySystemChangeRoot",
		KernelName: "CAP_SYS_CHROOT",
		ParserName: "CAP_SYS_CHROOT",
	},
	{
		Value:      CapabilitySystemProcessTrace,
		Name:       "sys-ptrace",
		GoName:     "CapabilitySystemProcessTrace",
		KernelName: "CAP_SYS_PTRACE",
		ParserName: "CAP_SYS_PTRACE",
	},
	{
		Value:      CapabilitySystemProcessAccounting,
		Name:       "sys-pacct",
		GoName:     "CapabilitySystemProcessAccounting",
		KernelName: "CAP_SYS_PACCT",
		ParserName: "CAP_SYS_PACCT",
	},
	{
		Value:      CapabilitySystemAdmin,
		Name:       "sys-admin",
		GoName:     "CapabilitySystemAdmin",
		KernelName: "CAP_SYS_ADMIN",
		ParserName: "CAP_SYS_ADMIN",
	},
	{
		Value:      CapabilitySystemBoot,
		Name:       "sys-boot",
		GoName:     "CapabilitySystemBoot",
		KernelName: "CAP_SYS_BOOT",
		ParserName: "CAP_SYS_BOOT",
	},
	{
		Value:      CapabilitySystemNice,
		Name:       "sys-nice",
		GoName:     "CapabilitySystemNice",
		KernelName: "CAP_SYS_NICE",
		ParserName: "CAP_SYS_NICE",
	},
	{
		Value:      CapabilitySystemResource,
		Name:       "sys-resource",
		GoName:     "CapabilitySystemResource",
		KernelName: "CAP_SYS_RESOURCE",
		ParserName: "CAP_SYS_RESOURCE",
	},
	{
		Value:      CapabilitySystemTime,
		Name:       "sys-time",
		GoName:     "CapabilitySystemTime",
		KernelName: "CAP_SYS_TIME",
		ParserName: "CAP_SYS_TIME",
	},
	{
		Value:      CapabilitySystemTerminalConfig,
		Name:       "sys-tty-config",
		GoName:     "CapabilitySysTerminalConfig",
		KernelName: "CAP_SYS_TTY_CONFIG",
		ParserName: "CAP_SYS_TTY_CONFIG",
	},
	{
		Value:      CapabilityMakeNode,
		Name:       "mknod",
		GoName:     "CapabilityMakeNode",
		KernelName: "CAP_MKNOD",
		ParserName: "CAP_MKNOD",
	},
	{
		Value:      CapabilityLease,
		Name:       "lease",
		GoName:     "CapabilityLease",
		KernelName: "CAP_LEASE",
		ParserName: "CAP_LEASE",
	},
	{
		Value:      CapabilityAuditWrite,
		Name:       "audit-write",
		GoName:     "CapabilityAuditWrite",
		KernelName: "CAP_AUDIT_WRITE",
		ParserName: "CAP_AUDIT_WRITE",
	},
	{
		Value:      CapabilityAuditControl,
		Name:       "audit-control",
		GoName:     "CapabilityAuditControl",
		KernelName: "CAP_AUDIT_CONTROL",
		ParserName: "CAP_AUDIT_CONTROL",
	},
	{
		Value:      CapabilitySetFileCapabilities,
		Name:       "setfcap",
		GoName:     "CapabilitySetFileCapabilities",
		KernelName: "CAP_SETFCAP",
		ParserName: "CAP_SETFCAP",
	},
	{
		Value:      CapabilityMandatoryAccessControlOverride,
		Name:       "mac-override",
		GoName:     "CapabilityMandatoryAccessControlOverride",
		KernelName: "CAP_MAC_OVERRIDE",
		ParserName: "CAP_MAC_OVERRIDE",
	},
	{
		Value:      CapabilityMandatoryAccessControlAdmin,
		Name:       "mac-admin",
		GoName:     "CapabilityMandatoryAccessControlAdmin",
		KernelName: "CAP_MAC_ADMIN",
		ParserName: "CAP_MAC_ADMIN",
	},
	{
		Value:      CapabilitySystemLog,
		Name:       "syslog",
		GoName:     "CapabilitySystemLog",
		KernelName: "CAP_SYSLOG",
		ParserName: "CAP_SYSLOG",
	},
	{
		Value:      CapabilityWakeAlarm,
		Name:       "wake-alarm",
		GoName:     "CapabilityWakeAlarm",
		KernelName: "CAP_WAKE_ALARM",
		ParserName: "CAP_WAKE_ALARM",
	},
	{
		Value:      CapabilityBlockSuspend,
		Name:       "block-suspend",
		GoName:     "CapabilityBlockSuspend",
		KernelName: "CAP_BLOCK_SUSPEND",
		ParserName: "CAP_BLOCK_SUSPEND",
	},
	{
		Value:      CapabilityAuditRead,
		Name:       "audit-read",
		GoName:     "CapabilityAuditRead",
		KernelName: "CAP_AUDIT_READ",
		ParserName: "CAP_AUDIT_READ",
	},
	{
		Value:      CapabilityPerformanceMonitor,
		Name:       "perfmon",
		GoName:     "CapabilityPerformanceMonitor",
		KernelName: "CAP_PERFMON",
		ParserName: "CAP_PERFMON",
	},
	{
		Value:      CapabilityBPF,
		Name:       "bpf",
		GoName:     "CapabilityBPF",
		KernelName: "CAP_BPF",
		ParserName: "CAP_BPF",
	},
	{
		Value:      CapabilityCheckpointRestore,
		Name:       "checkpoint-restore",
		GoName:     "CapabilityCheckpointRestore",
		KernelName: "CAP_CHECKPOINT_RESTORE",
		ParserName: "CAP_CHECKPOINT_RESTORE",
	},
}

var capabilitySetValues = internal.FlagFromEnum[CapabilityNumber, CapabilitySet](capabilityNumberValues)
