// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

// Package enumtest contains functions for testing EnumValues.
package enumtest

import (
	"testing"

	"gitlab.com/zygoon/go-apparmor/internal"
)

// Sane ensures that the given enumeration has no duplicate values or names.
func Sane[T internal.Unsigned](t *testing.T, values internal.EnumValues[T]) {
	t.Helper()

	ensureUniqueFieldValues(t, values, func(v internal.EnumValue[T]) T { return v.Value })
	ensureUniqueFieldValues(t, values, func(v internal.EnumValue[T]) string { return v.Name })
	ensureUniqueFieldValues(t, values, func(v internal.EnumValue[T]) string { return v.KernelName })
	ensureUniqueFieldValues(t, values, func(v internal.EnumValue[T]) string { return v.ParserName })
	ensureUniqueFieldValues(t, values, func(v internal.EnumValue[T]) string { return v.GoName })
}

// ensureUniqueFieldValues ensures that a field has unique values in a given slice.
//
// The access function is used to extract the field from each element of the slice.
// Items may be of any type, fields must be comparable.
func ensureUniqueFieldValues[T any, FT comparable](t *testing.T, items []T, access func(T) FT) {
	t.Helper()

	if err := internal.UniqueFieldValues[T, FT](items, access); err != nil {
		t.Fatal(err)
	}
}
