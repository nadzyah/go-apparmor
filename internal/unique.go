// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package internal

import (
	"fmt"
)

// UniqueFieldValues fails if the slice has non-unique values.
//
// Values are accessed with an access function, allowing inspection of specific structure field.
func UniqueFieldValues[T any, FT comparable](items []T, access func(T) FT) error {
	seen := make(map[FT]int, len(items))

	for idx := range items {
		val := access(items[idx])
		if oldIdx, ok := seen[val]; ok {
			return fmt.Errorf("value first seen at index %d repeated at index %d: %#v", oldIdx, idx, val)
		}

		seen[val] = idx
	}

	return nil
}
