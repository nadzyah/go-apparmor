// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

// Package flagtest contains functions for testing FlagValues.
package flagtest

import (
	"testing"

	"gitlab.com/zygoon/go-apparmor/internal"
)

// Sane ensures that the given flag set has no duplicate masks or names, or zero masks.
func Sane[T internal.Unsigned](t *testing.T, values internal.FlagValues[T]) {
	t.Helper()

	for idx := range values {
		if values[idx].Mask == 0 {
			t.Fatalf("Mask at index %d (%s) is zero, this is requires custom logic", idx, values[idx].Name)
		}
	}

	ensureUniqueFieldValues(t, values, func(v internal.FlagValue[T]) T { return v.Mask })
	ensureUniqueFieldValues(t, values, func(v internal.FlagValue[T]) string { return v.Name })
	ensureUniqueFieldValues(t, values, func(v internal.FlagValue[T]) string { return v.KernelName })
	ensureUniqueFieldValues(t, values, func(v internal.FlagValue[T]) string { return v.ParserName })
	ensureUniqueFieldValues(t, values, func(v internal.FlagValue[T]) string { return v.GoName })
}

// ensureUniqueFieldValues ensures that a field has unique values in a given slice.
//
// The access function is used to extract the field from each element of the slice.
// Items may be of any type, fields must be comparable.
func ensureUniqueFieldValues[T any, FT comparable](t *testing.T, items []T, access func(T) FT) {
	t.Helper()

	if err := internal.UniqueFieldValues[T, FT](items, access); err != nil {
		t.Fatal(err)
	}
}
