// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package internal_test

import (
	"testing"

	"gitlab.com/zygoon/go-apparmor/internal"
	"gitlab.com/zygoon/go-apparmor/internal/enumtest"
)

func TestEnumValues(t *testing.T) {
	const (
		Foo uint32 = iota
		Bar
	)

	values := internal.EnumValues[uint32]{
		internal.EnumValue[uint32]{
			Value:      Foo,
			Name:       "foo",
			KernelName: "AA_FOO",
			ParserName: "AA::Foo",
			GoName:     "Foo",
		}, {
			Value:      Bar,
			Name:       "bar",
			KernelName: "AA_BAR",
			ParserName: "AA::Bar",
			GoName:     "Bar",
		},
	}

	t.Run("Sane", func(t *testing.T) {
		enumtest.Sane(t, values)
	})

	t.Run("String", func(t *testing.T) {
		if n := values.String(Foo); n != "foo" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := values.String(Bar); n != "bar" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := values.String(0x5F); n != "0x5f" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})

	t.Run("GoString", func(t *testing.T) {
		if n := values.GoString(Foo); n != "Foo" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := values.GoString(Bar); n != "Bar" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := values.GoString(0x5F); n != "0x5f" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})

	t.Run("KernelString", func(t *testing.T) {
		if n := values.KernelString(Foo); n != "AA_FOO" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := values.KernelString(Bar); n != "AA_BAR" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := values.KernelString(0x5F); n != "0x5f" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})

	t.Run("ParserString", func(t *testing.T) {
		if n := values.ParserString(Foo); n != "AA::Foo" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := values.ParserString(Bar); n != "AA::Bar" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := values.String(0x5F); n != "0x5f" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})
}
