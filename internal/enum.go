// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package internal

import (
	"strconv"
)

// EnumValue describes a single value of a typed enumeration.
type EnumValue[T Unsigned] struct {
	Value      T      // Value is the actual numeric value.
	Name       string // Name is the human readable name of the value
	GoName     string // GoName is the Go symbol name, it is used in GoString.
	KernelName string // KernelName is the corresponding symbol or macro from the Linux kernel.
	ParserName string // ParserName is the corresponding symbol or macro from the AppArmor parser.
}

// EnumValues is a slice of EnumValue values.
type EnumValues[T Unsigned] []EnumValue[T]

// String finds the name matching the given value.
func (values EnumValues[T]) String(v T) string {
	return values.stringFunc(v, func(val EnumValue[T]) string { return val.Name })
}

// KernelString finds the kernel symbol name matching the given value.
func (values EnumValues[T]) KernelString(v T) string {
	return values.stringFunc(v, func(val EnumValue[T]) string { return val.KernelName })
}

// ParserString finds the AppArmor parser symbol name matching the given value.
func (values EnumValues[T]) ParserString(v T) string {
	return values.stringFunc(v, func(val EnumValue[T]) string { return val.ParserName })
}

// GoString finds the Go symbol name matching the given value.
func (values EnumValues[T]) GoString(v T) string {
	return values.stringFunc(v, func(val EnumValue[T]) string { return val.GoName })
}

func (values EnumValues[T]) stringFunc(v T, accessField func(n EnumValue[T]) string) string {
	for _, val := range values {
		if val.Value == v {
			return accessField(val)
		}
	}

	return fallback(uint64(v))
}

func fallback(v uint64) string {
	return "0x" + strconv.FormatUint(v, 16)
}
