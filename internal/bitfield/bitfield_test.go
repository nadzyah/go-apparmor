// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package bitfield_test

import (
	"testing"

	"gitlab.com/zygoon/go-apparmor/internal/bitfield"
)

func TestNumber(t *testing.T) {
	var num uint32

	if !bitfield.SetValue[uint32](bitfield.Number{Shift: 8, Width: 4}, &num, 15) {
		t.Fatal("Cannot fit value into bit-field")
	}

	if num != 0xf00 {
		t.Fatalf("Unexpected value: %#x", num)
	}

	if v := bitfield.Value[uint32](bitfield.Number{Shift: 8, Width: 4}, num); v != 15 {
		t.Fatalf("Unexpected value: %v", v)
	}

	if bitfield.SetValue[uint32](bitfield.Number{Shift: 8, Width: 4}, &num, 16) {
		t.Fatal("Overflow not detected")
	}
}

func TestBool(t *testing.T) {
	var num uint32

	bitfield.SetBoolValue(bitfield.Bool{Shift: 31}, &num, true)

	if num != 0x8000_0000 {
		t.Fatalf("Unexpected value: %#x", num)
	}

	if bitfield.BoolValue(bitfield.Bool{Shift: 31}, num) != true {
		t.Fatal("Unexpected false value")
	}

	if num != 0x8000_0000 {
		t.Fatalf("Unexpected value: %#x", num)
	}

	num = 0xffffffff

	bitfield.SetBoolValue(bitfield.Bool{Shift: 15}, &num, false)

	if num != 0xffff7fff {
		t.Fatalf("Unexpected value: %#x", num)
	}

	if bitfield.BoolValue(bitfield.Bool{Shift: 15}, num) != false {
		t.Fatal("Unexpected true value")
	}
}
