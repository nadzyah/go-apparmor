// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

// Package bitfield provides functions for manipulating bit-fields within unsigned numbers.
package bitfield

// Number is a definition of a bit-field with a given width and shift.
type Number struct {
	Width int
	Shift int
}

// Value returns the value of a given bit-field in the specified container.
func Value[valueT, containerT ~uint | ~uint8 | ~uint16 | ~uint32 | ~uint64](b Number, c containerT) valueT {
	return valueT(c&mask[containerT](b)) >> b.Shift
}

// SetValue sets the value of a given bit-field in the specified container.
//
// SetValue returns true if the given value fits without data-loss.
func SetValue[valueT, containerT ~uint | ~uint8 | ~uint16 | ~uint32 | ~uint64](b Number, c *containerT, v valueT) (ok bool) {
	if clamp(b, v) != v {
		return false
	}

	*c = *c&^mask[containerT](b) | containerT(v)<<b.Shift

	return true
}

// Bool is a definition of a one-bit-wide bit-field at a given shift.
type Bool struct {
	Shift int
}

// BoolValue returns the value of a given boolean bit-field in the specified container.
func BoolValue[containerT ~uint | ~uint8 | ~uint16 | ~uint32 | ~uint64](b Bool, c containerT) bool {
	return c&boolMask[containerT](b) != 0
}

// SetBoolValue sets the value of a given boolean bit-field in the specified container.
func SetBoolValue[containerT ~uint | ~uint8 | ~uint16 | ~uint32 | ~uint64](b Bool, c *containerT, v bool) {
	if v {
		*c = *c&^boolMask[containerT](b) | 1<<b.Shift
	} else {
		*c = *c &^ boolMask[containerT](b)
	}
}

func mask[containerT ~uint | ~uint8 | ~uint16 | ~uint32 | ~uint64](b Number) containerT {
	return (containerT(1)<<b.Width - 1) << b.Shift
}

func boolMask[containerT ~uint | ~uint8 | ~uint16 | ~uint32 | ~uint64](b Bool) containerT {
	return containerT(1) << b.Shift
}

func clamp[valueT ~uint | ~uint8 | ~uint16 | ~uint32 | ~uint64](b Number, v valueT) valueT {
	return v & (1<<b.Width - 1)
}
