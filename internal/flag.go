// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package internal

import (
	"fmt"
	"strings"
)

// FlagValue describes a single value of a typed flag set.
type FlagValue[T Unsigned] struct {
	Mask       T      // Mask of possibly more than one flag.
	Name       string // Name is the human readable name of the value
	GoName     string // GoName is the Go symbol name, it is used in GoString.
	KernelName string // KernelName is the corresponding symbol or macro from the Linux kernel.
	ParserName string // ParserName is the corresponding symbol or macro from the AppArmor parser.
}

// FlagValues is a slice of FlagValue values.
type FlagValues[T Unsigned] []FlagValue[T]

// FlagFromEnum creates FlagValues from EnumValues where the mask of each flag is 1<<EnumValue.Value.
func FlagFromEnum[TE Unsigned, TF Unsigned](ev EnumValues[TE]) FlagValues[TF] {
	fv := make(FlagValues[TF], len(ev))

	const (
		// shiftFmt is the shift format for Go.
		shiftFmt = "1<<%s"
		// shiftBraceFmt is the shift format for C/C++ where shift does not bind as tightly.
		shiftBraceFmt = "(1<<%s)"
	)

	for i := range ev {
		fv[i].Mask = TF(1) << TF(ev[i].Value)
		fv[i].GoName = fmt.Sprintf(shiftFmt, ev[i].GoName)
		fv[i].KernelName = fmt.Sprintf(shiftBraceFmt, ev[i].KernelName)
		fv[i].ParserName = fmt.Sprintf(shiftBraceFmt, ev[i].ParserName)
		fv[i].Name = ev[i].Name
	}

	return fv
}

// String builds a list of names matching the given value.
//
// Subsequent flag masks are matched against the flag set value.
// Any matching mask contributes to the produced name. Matched
// masks are subtracted from the local copy of the flag set value.
// Any remaining value is represented as a hexadecimal number.
func (names FlagValues[T]) String(f T) string {
	return names.stringFunc(f, ',', func(n FlagValue[T]) string {
		return n.Name
	})
}

// GoString builds a Go expression representing the given value.
func (names FlagValues[T]) GoString(f T) string {
	return names.stringFunc(f, '|', func(n FlagValue[T]) string {
		return n.GoName
	})
}

// KernelString builds a C expression representing the given value using kernel constants.
func (names FlagValues[T]) KernelString(f T) string {
	return names.stringFunc(f, '|', func(n FlagValue[T]) string {
		return n.KernelName
	})
}

// ParserString builds a C expression representing the given value using parser constants.
func (names FlagValues[T]) ParserString(f T) string {
	return names.stringFunc(f, '|', func(n FlagValue[T]) string {
		return n.ParserName
	})
}

func (names FlagValues[T]) stringFunc(f T, glue rune, accessField func(n FlagValue[T]) string) string {
	var sb strings.Builder

	for _, n := range names {
		if n.Mask&f == n.Mask {
			f &= ^n.Mask
			// This lets us do combined names that depend on order,
			// like O_RDWR instead of O_RDONLY|O_WRONLY.
			if sb.Len() > 0 {
				sb.WriteRune(glue)
			}

			sb.WriteString(accessField(n))
		}
	}

	if f != 0 {
		if sb.Len() > 0 {
			sb.WriteRune(glue)
		}

		sb.WriteString(fallback(uint64(f)))
	}

	if sb.Len() == 0 {
		sb.WriteRune('0')
	}

	return sb.String()
}
