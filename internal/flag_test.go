// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package internal_test

import (
	"testing"

	"gitlab.com/zygoon/go-apparmor/internal"
	"gitlab.com/zygoon/go-apparmor/internal/enumtest"
	"gitlab.com/zygoon/go-apparmor/internal/flagtest"
)

func TestFlagNames(t *testing.T) {
	const (
		ReadOnly  = 0x1
		WriteOnly = 0x2
		ReadWrite = ReadOnly | WriteOnly
		Append    = 0x4
	)

	values := internal.FlagValues[uint32]{{
		Mask:       ReadWrite,
		Name:       "read-write",
		GoName:     "ReadWrite",
		KernelName: "O_RDWR",
		ParserName: "O_RDWR",
	}, {
		Mask:       ReadOnly,
		Name:       "read",
		GoName:     "ReadOnly",
		ParserName: "O_RDONLY",
		KernelName: "O_RDONLY",
	}, {
		Mask:       WriteOnly,
		Name:       "write",
		GoName:     "WriteOnly",
		KernelName: "O_WRONLY",
		ParserName: "O_WRONLY",
	}, {
		Mask:       Append,
		Name:       "append",
		GoName:     "Append",
		KernelName: "O_APPEND",
		ParserName: "O_APPEND",
	}}

	t.Run("Sane", func(t *testing.T) {
		flagtest.Sane(t, values)
	})

	t.Run("String", func(t *testing.T) {
		if n := values.String(ReadWrite); n != "read-write" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := values.String(ReadOnly | WriteOnly); n != "read-write" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := values.String(WriteOnly); n != "write" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := values.String(WriteOnly | Append); n != "write,append" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := values.String(WriteOnly | 0x8); n != "write,0x8" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := values.String(0); n != "0" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})

	t.Run("GoString", func(t *testing.T) {
		if n := values.GoString(ReadWrite); n != "ReadWrite" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := values.GoString(ReadOnly | WriteOnly); n != "ReadWrite" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := values.GoString(WriteOnly); n != "WriteOnly" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := values.GoString(WriteOnly | Append); n != "WriteOnly|Append" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := values.GoString(WriteOnly | 0x8); n != "WriteOnly|0x8" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})

	t.Run("KernelString", func(t *testing.T) {
		if n := values.KernelString(ReadWrite); n != "O_RDWR" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := values.KernelString(ReadOnly | WriteOnly); n != "O_RDWR" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := values.KernelString(WriteOnly); n != "O_WRONLY" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := values.KernelString(WriteOnly | Append); n != "O_WRONLY|O_APPEND" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := values.KernelString(WriteOnly | 0x8); n != "O_WRONLY|0x8" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})

	t.Run("ParserString", func(t *testing.T) {
		if n := values.ParserString(ReadWrite); n != "O_RDWR" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := values.ParserString(ReadOnly | WriteOnly); n != "O_RDWR" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := values.ParserString(WriteOnly); n != "O_WRONLY" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := values.ParserString(WriteOnly | Append); n != "O_WRONLY|O_APPEND" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := values.ParserString(WriteOnly | 0x8); n != "O_WRONLY|0x8" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})
}

func TestFlagFromEnum(t *testing.T) {
	const (
		CapFoo uint32 = iota
		CapBar
	)

	enumValues := internal.EnumValues[uint32]{
		internal.EnumValue[uint32]{
			Value:      CapFoo,
			Name:       "foo",
			KernelName: "CAP_FOO",
			ParserName: "Cap::Foo",
			GoName:     "CapFoo",
		}, {
			Value:      CapBar,
			Name:       "bar",
			KernelName: "CAP_BAR",
			ParserName: "Cap::Bar",
			GoName:     "CapBar",
		},
	}

	enumtest.Sane(t, enumValues)

	flagValues := internal.FlagFromEnum[uint32, uint64](enumValues)

	flagtest.Sane(t, flagValues)

	if n := flagValues.String(1<<CapBar | 1<<CapFoo); n != "foo,bar" {
		t.Fatalf("Unexpected name: %q", n)
	}

	if n := flagValues.GoString(1<<CapBar | 1<<CapFoo); n != "1<<CapFoo|1<<CapBar" {
		t.Fatalf("Unexpected name: %q", n)
	}

	if n := flagValues.KernelString(1<<CapBar | 1<<CapFoo); n != "(1<<CAP_FOO)|(1<<CAP_BAR)" {
		t.Fatalf("Unexpected name: %q", n)
	}

	if n := flagValues.ParserString(1<<CapBar | 1<<CapFoo); n != "(1<<Cap::Foo)|(1<<Cap::Bar)" {
		t.Fatalf("Unexpected name: %q", n)
	}
}
