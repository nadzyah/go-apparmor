<!--
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: Canonical Ltd
-->
# Overview

AppArmor is built out of two distinct formats. Most of the file contains
little-endian, tagged data, with the usual mix of unsigned integers,
length-prefixed strings and blobs, as well as paired (open-close pair) arrays of
arbitrary values and structures with arbitrary fields. Many fields are prefixed
with a _name_, allowing for optional elements, while making parsing logic
straightforward.

Deep inside that format, encoded as a tagged blob, is another format: untagged
big-endian data, several flags that make it possible to advance the decode
process and to know the width of the remaining fields.

The format of each binary produced by `apparmor_parser` and consumed by the
Linux kernel is thus:

- Header
- First binary profile
- (optional) subsequent binary profiles

The header contains the ABI version, currently version 7 being commonly
encountered in the field. This is emphasized to differentiate the relatively
little-known number from the commonly seen "abi/3.0" or "abi/4.0" files used by
the parser. The meaning is quite different.

**BUGs**: At least one value differs between the user-space profile parser and
compiler and the kernel which parses the binary. Since the kernel is the root
of ultimate behavior, we err on the side of the kernel. Any such case is
highlighted as it is most likely a bug that was not noticed before.

# Little endian envelope

The following section describes the _envelope_ containing everything apart from
the state machine (DFA) data. As indicated by the section title, all integers
encountered here are stored in little endian byte-order.

# Tagged values

There are relatively few untagged values in the envelope. Almost everything is
encoded as a single byte followed by the content that may be deduced from the
byte. The table contains kernel names which may be helpful as a cross-reference.
Inside the kernel bytes are of type `enum aa_code`.

| Value | Name            | Description              | Kernel Name    |
| ----- | --------------- | ------------------------ | -------------- |
| 0     | U8              | `uint8`                  | `AA_U8`        |
| 1     | U16             | `uint16` (little-endian) | `AA_U16`       |
| 2     | U32             | `uint32` (little-endian) | `AA_U32`       |
| 3     | U64             | `uint64` (little-endian) | `AA_U64`       |
| 4     | Name            | Symbol name              | `AA_NAME`      |
| 5     | String          | NUL terminated           | `AA_STRING`    |
| 6     | Blob            | Arbitrary bytes          | `AA_BLOB`      |
| 7     | Start Structure | Start of structure       | `AA_STRUCT`    |
| 8     | End Structure   |                          | `AA_STRUCTEND` |
| 9     | Start List      | Unused                   | `AA_LIST`      |
| 10    | End List        |                          | `AA_LISTEND`   |
| 11    | Start Array     | Start of an array        | `AA_ARRAY`     |
| 12    | End Array       |                          | `AA_ARRAYEND`  |

Some notes:
- All integer quantities are little endian, regardless of CPU byte order.
- Most variable-size values (_Name_, _String_, _Array_ and _List_)
  are preceded by a 16 bit length (U16). _Blob_ uses 32 bit length.
- Both _Name_ and _String_ are NUL-terminated strings.
- _Name_ is used for naming symbols or structure fields.
- _String_ is used as a regular value.
- _List_ is unused and not supported by the kernel.

## Header

The header has the following format. The _Name_ column indicates which named
symbol (`AA_NAME`) precedes the value. Optional values mean that neither the
symbol name nor the tagged value are present in the file.

| Name          | Type        | Description                      | Notes    |
| ------------- | ----------- | -------------------------------- | -------- |
| `"version"`   | `AA_U32`    | Packed version and flags         |          |
| `"namespace"` | `AA_STRING` | Namespace the profile belongs to | Optional |

The version field can be masked to obtain more information:

| Mask  | Meaning                             |
| ----- | ----------------------------------- |
| 0x3ff | ABI version                         |
| 0x800 | Force complain flag on all profiles |

## Profiles

Immediately after the header is first profile. Profiles continue up until the
end of the file. There's no indication as to how many profiles to expect.  ABI
version allows the parser to understand what to expect inside each profile.
Commonly seen values include 5, and 7.

## Profile

The profile is a somewhat large structure with lots of fields and some unusual
encoding rooted at evolutionary design. Many elements are optional. Some pieces
of data are decoded and ignored. Some elements are required when other elements
are present. Those are indicated by the syntax `Required (xxx)` where `xxx` is
the name of an earlier element that controls if the current element is decoded.

The _Name_ column indicates which named symbol (`AA_NAME`) precedes the value.
Optional values mean that neither the symbol name nor the tagged value are
present in the file.

| Name               | Type            | Description               | Notes             |
| ------------------ | --------------- | ------------------------- | ----------------- |
| `"profile"`        | `AA_STRUCT`     |                           |                   |
|                    | `AA_STRING`     | Name of the profile       |                   |
| `"rename"`         | `AA_STRING`     | New name of the profile   | Optional          |
| `"attach"`         | `AA_STRING`     | Attachment string?        | Optional          |
|                    | `AA_STRUCT`     |                           | Required (attach) |
|                    | Policy Database | x-match policy database   | Required (attach) |
|                    | `AA_U32`        | x-match length            | Required (attach) |
|                    | `AA_STRUCTEND`  | End of unnamed structure  |                   |
| `"disconnected"`   | `AA_STRING`     | ?                         | Optional          |
| `"kill"`           | `AA_U32`        | Kill signal               | Optional, SAUCE   |
| `"flags"`          | `AA_U32`        | Profile flags             |                   |
|                    | `AA_U32`        | Profile mode              |                   |
|                    | `AA_U32`        | Audit mode                |                   |
|                    | `AA_STRUCTEND`  | End of `"flags"`          |                   |
| `"path_flags"`     | `AA_U32`        | ?                         | Optional          |
|                    | `AA_U32`        | Caps: allow               |                   |
|                    | `AA_U32`        | Caps: audit               |                   |
|                    | `AA_U32`        | Caps: quiet               |                   |
|                    | `AA_U32`        | Ignored                   |                   |
| `"caps64"`         | `AA_STRUCT`     | High-bits of capabilities | Optional          |
|                    | `AA_U32`        | Caps: allow (high)        |                   |
|                    | `AA_U32`        | Caps: audit (high)        |                   |
|                    | `AA_U32`        | Caps: quiet (high)        |                   |
|                    | `AA_U32`        | Ignored                   |                   |
|                    | `AA_STRUCTEND`  | End of `"caps64"`         |                   |
| `"capsx"`          | `AA_STRUCT`     | Extended capabilities     | Optional          |
|                    | `AA_U32`        | Low bits                  |                   |
|                    | `AA_U32`        | High bits                 |                   |
|                    | `AA_STRUCTEND`  | End of `"capsx"`          |                   |
| `"xattrs"`         | `AA_STRUCT`     | Extended attributes       | Optional          |
|                    | `AA_ARRAY`      |                           |                   |
|                    | `AA_STRING`     | Attribute name=value      |                   |
|                    | `AA_ARRAYEND`   |                           |                   |
|                    | `AA_STRUCTEND`  | End of `"xattrs"`         |                   |
| `"rlimits"`        | `AA_STRUCT`     | Runtime limits            | Optional          |
|                    | `AA_U32`        | Mask ?                    |                   |
|                    | `AA_ARRAY`      | Array of limits           |
|                    | `AA_ARRAYEND`   |                           |                   |
|                    | `AA_STRUCTEND`  | End of `"rlimits"`        |                   |
| `"secmark"`        | `AA_STRUCT`     | Security mark             | Optional          |
|                    | `AA_ARRAY`      | Array of entries          |
|                    | `AA_U8`         | Audit value               |
|                    | `AA_U8`         | Deny value                |
|                    | `AA_STRING`     | Security label            |                   |
|                    | `AA_ARRAYEND`   |                           |                   |
|                    | `AA_STRUCTEND`  | End of `"secmark"`        |                   |
| `"net_allowed_af"` | `AA_ARRAY`      | For each `AF_FAMILY`      | Version 5-7       |
|                    | `AA_U16`        | Allow                     |                   |
|                    | `AA_U16`        | Audit                     |                   |
|                    | `AA_U16`        | Quiet                     |                   |
|                    | `AA_ARRAYEND`   | End of `"net_allowed_af"` |                   |
| `"policydb"`       | Policy Database | Policy for non-files      | Optional          |
|                    | `AA_STRUCTEND`  | End of `"policydb"`       |                   |
|                    | Policy DataBase | Policy for files          |                   |
| `"data"`           | `AA_ARRAY`      | Hash table of string:blob |                   |
|                    | `AA_STRING`     | Entry name (key)          |                   |
|                    | `AA_BLOB`       | Entry value               |                   |
|                    | `AA_ARRAYEND`   | End of `"data"`           |                   |
|                    | `AA_STRUCTEND`  | End of `"profile"`        |                   |

- The whole profile is packed inside one big structure. This makes it somewhat
  easier to detect correctly parsed or mis-parsed data in case the format description
  is incorrect or future AppArmor parser and kernel extend it with additional fields.
- Profile name contains syntax to encode both the namespace name and the name within
  the namespace. Note that AppArmor namespace are unlike typical kernel name spaces
  so it is best not to make assumptions based on existing knowledge.
- The `"rename"` string allows a to load (and replace) a profile under one name and
  give the resulting profile a different name.
- The `"attach"` attachment string is optional but when present the `"xmatch"` data
  structure and the following length become mandatory.
- Data within `"aadfa"` blob is aligned to 8-byte boundary. You may have to skip
  up to seven leading zeros to get to the first byte of actual content.
- The `"disconnected"` string controls how the profile attaches to paths with different
  mount namespaces.
- The `"flags"` structure contains flags, enforcement mode and an audit flag (zero
  or non-zero). The most useful property that flags may encode is that a profile
  is really a _hat_ profile. The mode governs how the profile is enforced by the
  kernel.  Note that header-wide force-complain bit packed into the header
  version field overrides the mode in all the profiles in the same binary.
- The `"path_flags"` field controls certain mechanisms related to paths:
  chroot-relative, mediation of deleted files, profile attachment and namespace
  attachment.

### Profile Flags

Profile flags

| Mask | Flag Name     | Description        | Kernel Name                 |
| ---- | ------------- | ------------------ | --------------------------- |
| 1    | Hat           | Profile is a "hat" | `PACKED_FLAG_HAT`           |
| 2    | 1st Debug     | Unused             | `PACKED_FLAG_DEBUG1`        |
| 4    | 2nd Debug     | Unused             | `PACKED_FLAG_DEBUG2`        |
| 8    | Interruptible | SAUCE              | `PACKED_FLAG_INTERRUPTIBLE` |

- Hats are a special type of profile that a process may change during
  its runtime. AppArmor allows processes to transition to a named hat
  that is logically associated to the main profile.
- Debug flags are unused
- Interruptible flag is not upstream yet. It is related to the
  notification interface (prompting).

### Profile Mode

Each profile has a mode byte that roughly tells the kernel how failure to match
against the rules impacts the process. Note that _complain_ mode may be also indicated
globally in the header version-and-flags field.

| Value | Name       | Description     | Kernel Name              |
| ----- | ---------- | --------------- | ------------------------ |
| 0     | Enforce    | Enforce rules   | `PACKED_MODE_ENFORCE`    |
| 1     | Complain   | Log but allow   | `PACKED_MODE_COMPLAIN`   |
| 2     | Kill       | Kill violators  | `PACKED_MODE_KILL`       |
| 3     | Unconfined | No restrictions | `PACKED_MODE_UNCONFINED` |
| 4     | User       | ?               | `PACKED_MODE_USER`       |

- By default nothing is allowed and nothing is denied.
- _Complain_ mode sends _audit_ events but allows everything that is not denied.
- _Kill_ mode kills the process that steps out of the allowed set.
- _Unconfined_ mode gives a process a named profile without any restrictions.
- _User_ mode is not implemented and is related to user-space notifications.

### Audit mode

This field acts as a boolean even though it is encoded as `uint32`. The parser
always sets this to 1. The kernel decodes this as `AUDIT_ALL`. In theory it
could be used to provide more precise audit flags.

### Path Flags

Path flags is a bit-field with the following flags. Note that the mask is the
value encoded in the binary, not the in-memory representation in the parser,
which are irrelevant to the kernel and are actually different.

| Mask    | Flag Name               | Profile syntax          | Parser Name            | Kernel Name             |
| ------- | ----------------------- | ----------------------- | ---------------------- | ----------------------- |
| 0x4     | Attach                  | `"attach_disconnected"` | `PATH_ATTACH`          | `PATH_CONNECT_PATH`     |
| 0x8     | Chroot relative         | `"chroot_relative"`     | `PATH_CHROOT_REL`      | `PATH_CHROOT_REL`       |
| 0x10    | Chroot namespace attach | `"chroot_attach"`       | `PATH_CHROOT_NSATTACH` | `PATH_CHROOT_NSCONNECT` |
| 0x10000 | ?                       | `"mediate_deleted"`     | Bug!                   | `PATH_DELEGATE_DELETED` |
| 0x20000 | ?                       | `"delegate_deleted"`    | Bug!                   | `PATH_DELEGATE_DELETED` |

- The parser recognized `"audit"`, `"choot_relative"`, `"namespace_relative"`,
  `"mediate_deleted"`, `"delegate_deleted"`, `"attach_disconnected"`,
  `"no_attach_disconnected"`, `"chroot_attach"`, `"chroot_no_attach"`,
  `"attach_disconnected.path=VALUE"`, `"kill.signal=VALUE"` and
  `"interruptible"` strings as a profile flag. Some of those are encoded as
  other flags though.
- The kernel respects `PATH_CHROOT_REL`, `PATH_MEDIATE_DELETED`, `PATH_DELEGATE_DELETED`.
- **BUG**: The kernel and the parser disagree on some values. Kernel's
  `PATH_DELEGATE_DELETED` is 0x10000 which corresponds to `PATH_MEDIATE_DELETED`
  in the parser. The parser never uses `PATH_DELEGATE_DELETED`.

### Capabilities

Capabilities are encoded in three elements: mandatory inline values and two
optional structures: one adding high-bits and one encoding both low and high
bits of extended capabilities.

The purpose of extended capabilities is unclear.

### Extended attributes

The kernel can attach a profile based on the executable name and the set of
extended attributes present on the executable.

### Runtime limits

### Security mark

TBD

### Policy database

The policy database is a recurring component that exists in up to three places
in each profile. The extended attribute match, the policy database proper and
the file-specific policy database.

The database has optional permissions table, followed by an optional DFA, followed
by the DFA start state if DFA was present, followed by execution transition table.

| Name          | Type           | Description                     | Notes       |
| ------------- | -------------- | ------------------------------- | ----------- |
| `"perms "`    | `AA_STRUCT`    |                                 | Optional    |
| `"version"`   | `AA_U32`       | Version of the permission table |             |
|               | `AA_ARRAY `    | Array of permissions            | (version=1) |
|               | `AA_U32`       | Allow permission set (1st copy) | Ignored     |
|               | `AA_U32`       | Allow permission set            |             |
|               | `AA_U32`       | Deny permission set             |             |
|               | `AA_U32`       | Sub-tree permission set         |             |
|               | `AA_U32`       | Conditional permission set (?)  |             |
|               | `AA_U32`       | Kill permission set             |             |
|               | `AA_U32`       | Complain permission set         |             |
|               | `AA_U32`       | Prompt permission set           |             |
|               | `AA_U32`       | Audit permission set            |             |
|               | `AA_U32`       | _Quiet_ permission set          |             |
|               | `AA_U32`       | _Hide_ permission set           |             |
|               | `AA_U32`       | X-index (?)                     |             |
|               | `AA_U32`       | Tag index (?)                   |             |
|               | `AA_U32`       | Label index (?)                 |             |
|               | `AA_ARRAYEND ` |                                 |             |
| `"aadfa"`     | `AA_BLOB`      | Data is aligned to 8-bytes      | Optional    |
| `"start"`     | `AA_U32`       | Start state for "none" DFA      | Optional    |
| `"dfa_start"` | `AA_U32`       | Start state for "file" DFA      | Optional    |
| `"xtable"`    | `AA_STRUCT`    | Exec/transition table           | Optional    |
|               | `AA_ARRAY `    | Array of entries                |             |
|               | `AA_STRING`    | NUL-stuffed string with rules   |             |
|               | `AA_ARRAYEND ` |                                 |             |

- The version field describes how to decode the permission bits.
- All permission fields are unnamed but are all tagged as `AA_U32`.
- The _allow_ permission set is encoded twice. The first copy is decoded and
  immediately overwritten by the second copy.
- The executable transition table contains strings with embedded NUL bytes
  which follow a specific format.

### Network rules

In ABI versions smaller than 8, network rules are encoded in the
`"net_allowed_af`" structure. In ABI versions starting with 8, all network
rules are encoded in the policy database as the `AA_CLASS_NET` class in the
table of DFAs.

### Signal rules

Signal rules are encoded in the policy database, as the `AA_CLASS_SIGNAL` class
in the table of DFAs.

### Key=value Data

TBD
