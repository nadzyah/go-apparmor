// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package apparmor_test

import (
	"testing"

	"gitlab.com/zygoon/go-apparmor"
	"gitlab.com/zygoon/go-apparmor/internal/enumtest"
)

func TestSignalNumber(t *testing.T) {
	if l := len(apparmor.SignalNumberValues); l != 35 {
		t.Fatalf("Unexpected number of signal number values: %d", l)
	}

	t.Run("sane", func(t *testing.T) {
		enumtest.Sane(t, apparmor.SignalNumberValues)
	})

	t.Run("sigquit", func(t *testing.T) {
		if n := apparmor.QuitSignal.String(); n != "quit" {
			t.Errorf("Unexpected name: %v", n)
		}

		if n := apparmor.QuitSignal.GoString(); n != "QuitSignal" {
			t.Errorf("Unexpected name: %v", n)
		}

		if n := apparmor.QuitSignal.KernelString(); n != "SIGQUIT" {
			t.Errorf("Unexpected name: %v", n)
		}

		if n := apparmor.QuitSignal.ParserString(); n != "SIGQUIT" {
			t.Errorf("Unexpected name: %v", n)
		}
	})

	t.Run("realtime+0", func(t *testing.T) {
		sig := apparmor.RealTimeSignal(0)

		if n := sig.String(); n != "rtmin+0" {
			t.Errorf("Unexpected name: %v", n)
		}

		if n := sig.GoString(); n != "RealTimeSignal(0)" {
			t.Errorf("Unexpected name: %v", n)
		}

		if n := sig.KernelString(); n != "SIGRTMIN+0" {
			t.Errorf("Unexpected name: %v", n)
		}

		if n := sig.ParserString(); n != "MINRT_SIG+0" {
			t.Errorf("Unexpected name: %v", n)
		}
	})

	t.Run("realtime+5", func(t *testing.T) {
		sig := apparmor.RealTimeSignal(5)

		if n := sig.String(); n != "rtmin+5" {
			t.Errorf("Unexpected name: %v", n)
		}

		if n := sig.GoString(); n != "RealTimeSignal(5)" {
			t.Errorf("Unexpected name: %v", n)
		}

		if n := sig.KernelString(); n != "SIGRTMIN+5" {
			t.Errorf("Unexpected name: %v", n)
		}

		if n := sig.ParserString(); n != "MINRT_SIG+5" {
			t.Errorf("Unexpected name: %v", n)
		}
	})

	t.Run("realtime+32", func(t *testing.T) {
		sig := apparmor.RealTimeSignal(32)

		if n := sig.String(); n != "rtmin+32" {
			t.Errorf("Unexpected name: %v", n)
		}

		if n := sig.GoString(); n != "RealTimeSignal(32)" {
			t.Errorf("Unexpected name: %v", n)
		}

		if n := sig.KernelString(); n != "SIGRTMIN+32" {
			t.Errorf("Unexpected name: %v", n)
		}

		if n := sig.ParserString(); n != "MINRT_SIG+32" {
			t.Errorf("Unexpected name: %v", n)
		}
	})
}
