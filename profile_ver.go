// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package apparmor

import (
	"errors"
	"fmt"

	"gitlab.com/zygoon/go-apparmor/internal/bitfield"
)

var (
	kernelAbiField     = bitfield.Number{Shift: 0, Width: 10}
	forceComplainField = bitfield.Bool{Shift: 11}
	parserAbiField     = bitfield.Number{Shift: 12, Width: 8}
	policyVersionField = bitfield.Number{Shift: 20, Width: 8}
)

var (
	errPolicyVersionRange = errors.New("policy version is too large")
	errKernelAbiRange     = errors.New("kernel ABI is too large")
	errParserAbiRange     = errors.New("parser ABI is too large")
)

// PackedProfileVersion combines kernel, parser and policy versions with the force-complain flag.
type PackedProfileVersion uint32

// String returns a human-readable representation of the packed profile version.
func (ppv PackedProfileVersion) String() string {
	return fmt.Sprintf("%#x (policy-version:%d, kernel-abi:%d, parser-abi:%d, force-complain:%v)",
		uint32(ppv), ppv.PolicyVersion(), ppv.KernelAbi(), ppv.ParserAbi(), ppv.ForceComplain())
}

// PolicyVersion returns the version of the implemented policy.
//
// The kernel uses this number to understand the format of data to expect,
// adjusting parsing routines.
func (ppv PackedProfileVersion) PolicyVersion() uint32 {
	return bitfield.Value[uint32](policyVersionField, ppv)
}

// SetPolicyVersion sets the version of the implemented policy.
func (ppv *PackedProfileVersion) SetPolicyVersion(v uint32) error {
	if !bitfield.SetValue(policyVersionField, ppv, v) {
		return errPolicyVersionRange
	}

	return nil
}

// ForceComplain returns true if the kernel should interpret all rules as non-enforcing.
func (ppv PackedProfileVersion) ForceComplain() bool {
	return bitfield.BoolValue(forceComplainField, ppv)
}

// SetForceComplain sets the force-complain flag.
func (ppv *PackedProfileVersion) SetForceComplain(v bool) {
	bitfield.SetBoolValue(forceComplainField, ppv, v)
}

// KernelAbi returns the version of the kernel-side ABI.
//
// The kernel uses this number to understand the capabilities (measured or
// declared) of the kernel at the time the parser had compiled the profile.
func (ppv PackedProfileVersion) KernelAbi() uint32 {
	return bitfield.Value[uint32](kernelAbiField, ppv)
}

// SetKernelAbi sets the version of the kernel-side ABI.
func (ppv *PackedProfileVersion) SetKernelAbi(v uint32) error {
	if !bitfield.SetValue(kernelAbiField, ppv, v) {
		return errKernelAbiRange
	}

	return nil
}

// ParserAbi returns the version of the parser-side ABI.
//
// The kernel uses this number to understand the capabilities of the parser
// which compiled the profile.
func (ppv PackedProfileVersion) ParserAbi() uint32 {
	return bitfield.Value[uint32](parserAbiField, ppv)
}

// SetParserAbi sets the version of the parser-side ABI.
func (ppv *PackedProfileVersion) SetParserAbi(v uint32) error {
	if !bitfield.SetValue(parserAbiField, ppv, v) {
		return errParserAbiRange
	}

	return nil
}
