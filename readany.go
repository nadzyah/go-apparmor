// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package apparmor

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io"

	"gitlab.com/zygoon/go-apparmor/internal"
)

// Errors that may be returned by ReadAny.
var (
	// ErrUnrecognizedTag records encounter with an unrecognized tag byte.
	ErrUnrecognizedTag = errors.New("unrecognized tag byte")
	// ErrInvalidSymbol records attempt to read a symbol that is not well-formed.
	ErrMalformedName = errors.New("malformed name")
	// ErrMalformedString records attempt to read a string that is not well-formed.
	ErrMalformedString = errors.New("malformed string")
	// ErrMalformedBlob records attempt to read a blob that is not well-formed.
	ErrMalformedBlob = errors.New("malformed blob")
)

// ReadAny reads a dynamic value from the given reader.
//
// Read will eventually fail with io.EOF. The caller is expected to check the
// error and not treat EOF as an error unless it violates the rules of structure
// and array nesting.
//
// The offset and the tag byte are returned even on error, unless the error
// prevented accessing the current position or reading the tag value.
//
// The returned offset is such, that seeking to that offset and calling ReadAny
// will produce the same reply. Depending on the tag, the offset of the first
// byte of the value may be different, as it must take into account the tag
// byte, string length (two bytes for TagName and TagString) or blob length
// (four bytes for TagBlob).
func ReadAny[R io.ReadSeeker](r R) (offset int64, tag TagByte, value any, err error) {
	pos, err := r.Seek(0, io.SeekCurrent)
	if err != nil {
		return 0, invalidTag, nil, err
	}

	tag, err = readRawLittleEndianUint[TagByte](r)
	if err != nil {
		return pos, invalidTag, nil, err
	}

	switch tag {
	case TagU8:
		v, err := readRawLittleEndianUint[uint8](r)
		if err != nil {
			return pos, tag, nil, errorWithPrefix("cannot read uint8: ", err)
		}

		return pos, tag, v, nil
	case TagU16:
		v, err := readRawLittleEndianUint[uint16](r)
		if err != nil {
			return pos, tag, nil, errorWithPrefix("cannot read uint16: ", err)
		}

		return pos, tag, v, nil
	case TagU32:
		v, err := readRawLittleEndianUint[uint32](r)
		if err != nil {
			return pos, tag, nil, errorWithPrefix("cannot read uint32: ", err)
		}

		return pos, tag, v, nil
	case TagU64:
		v, err := readRawLittleEndianUint[uint64](r)
		if err != nil {
			return pos, tag, nil, errorWithPrefix("cannot read uint64: ", err)
		}

		return pos, tag, v, nil
	case TagName:
		const prefix = "cannot read name: "

		textData, err := readRawChunk[uint16](r)
		if err != nil {
			return pos, tag, nil, errorWithPrefix(prefix, err)
		}

		if len(textData) == 0 || textData[len(textData)-1] != 0 {
			return pos, tag, nil, errorWithPrefix(prefix, ErrMalformedName)
		}

		v := string(textData[:len(textData)-1])

		return pos, tag, v, nil
	case TagString:
		const prefix = "cannot read string: "

		textData, err := readRawChunk[uint16](r)
		if err != nil {
			return pos, tag, nil, errorWithPrefix(prefix, err)
		}

		if len(textData) == 0 || textData[len(textData)-1] != 0 {
			return pos, tag, nil, errorWithPrefix(prefix, ErrMalformedString)
		}

		v := string(textData[:len(textData)-1])

		return pos, tag, v, nil
	case TagBlob:
		const prefix = "cannot read blob: "

		v, err := readRawChunk[uint32](r)
		if err != nil {
			return pos, tag, nil, errorWithPrefix(prefix, err)
		}

		return pos, tag, v, nil
	case TagListStart, TagListEnd:
		return pos, tag, nil, nil
	case TagArrayStart:
		v, err := readRawLittleEndianUint[uint16](r)
		if err != nil {
			return pos, tag, nil, errorWithPrefix("cannot read array length: ", err)
		}

		return pos, tag, v, nil
	case TagArrayEnd:
		return pos, tag, nil, nil
	case TagStructureStart, TagStructureEnd:
		return pos, tag, nil, nil
	default:
		return pos, tag, nil, ErrUnrecognizedTag
	}
}

// readRawLittleEndianUint reads an little-endian, unsigned integer of specific width.
func readRawLittleEndianUint[T internal.Unsigned, R io.Reader](r R) (v T, err error) {
	err = binary.Read(r, binary.LittleEndian, &v)

	return v, err
}

// readRawChunk reads a sized chunk with sizeT encoding the width of the size field.
//
// The returned chunk is nil when the size is zero.
func readRawChunk[sizeT internal.Unsigned, R io.Reader](r R) (chunk []byte, err error) {
	len, err := readRawLittleEndianUint[sizeT, R](r)
	if err != nil {
		return nil, err
	}

	if len > 0 {
		chunk = make([]byte, len)
		if _, err := io.ReadFull(r, chunk); err != nil {
			return nil, fmt.Errorf("cannot read chunk[%d]: %w", len, err)
		}
	}

	return chunk, nil
}

// prefixedError combines a static prefix with a dynamic error.
type prefixedError struct {
	Prefix string
	Err    error
}

func (e prefixedError) Error() string {
	return e.Prefix + e.Err.Error()
}

func (e prefixedError) Unwrap() error {
	return e.Err
}

func errorWithPrefix(prefix string, e error) error {
	return prefixedError{Prefix: prefix, Err: e}
}
