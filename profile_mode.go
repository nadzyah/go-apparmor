// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package apparmor

import "gitlab.com/zygoon/go-apparmor/internal"

// PackedProfileMode determines how the kernel reacts to the operations not allowed by a profile.
type PackedProfileMode uint32

// Known values of PackedProfileMode, as of Linux 6.8 and AppArmor 4.0 with
// several Ubuntu patches for user-space notification (aka prompting.)
const (
	// EnforceMode indicates that operations not allowed by the profile are
	// refused and EPERM is returned to userspace.
	EnforceMode PackedProfileMode = 0
	// ComplainMode indicates that operations not allowed by the profile are
	// allowed but an audit message is produced, allowing tools to extract the
	// set of violations to build a more complete profile.
	ComplainMode PackedProfileMode = 1
	// KillMode indicates that operations not allowed by the profile cause the
	// process to be killed. The specific signal may be selected with another
	// mechanism.
	KillMode PackedProfileMode = 2
	// UnconfinedMode indicates that all operations are allowed but the process
	// still carries a distinct label visible across UNIX domain sockets.
	UnconfinedMode PackedProfileMode = 3
	// UserMode indicates that part of the decision is relayed to user-space,
	// delivered as events over a special file in apparmorfs.
	UserMode PackedProfileMode = 4
)

// String returns the human-readable name of the mode.
func (v PackedProfileMode) String() string {
	return profileModeValues.String(v)
}

// GoString returns the corresponding Go constant or literal.
func (v PackedProfileMode) GoString() string {
	return profileModeValues.GoString(v)
}

// KernelString returns the corresponding Linux kernel constant or literal.
func (v PackedProfileMode) KernelString() string {
	return profileModeValues.KernelString(v)
}

// ParserString returns the corresponding AppArmor parser constant or literal.
func (v PackedProfileMode) ParserString() string {
	return profileModeValues.ParserString(v)
}

// profileModeValues contains known values for PackedProfileMode.
//
// AppArmor parser has another mode "MODE_DEFAULT_ALLOW" that is not
// implemented in the kernel.  Values used by the AppArmor parser in memory are
// greater by one than the values stored in the binary profile.
var profileModeValues = internal.EnumValues[PackedProfileMode]{
	{
		Value:      EnforceMode,
		Name:       "enforce",
		GoName:     "EnforceMode",
		KernelName: "PACKED_MODE_ENFORCE",
		ParserName: "MODE_PACKED",
	}, {
		Value:      ComplainMode,
		Name:       "complain",
		GoName:     "ComplainMode",
		KernelName: "PACKED_MODE_COMPLAIN",
		ParserName: "MODE_COMPLAIN",
	}, {
		Value:      KillMode,
		Name:       "kill",
		GoName:     "KillMode",
		KernelName: "PACKED_MODE_KILL",
		ParserName: "MODE_KILL",
	}, {
		Value:      UnconfinedMode,
		Name:       "unconfined",
		GoName:     "UnconfinedMode",
		KernelName: "PACKED_MODE_UNCONFINED",
		ParserName: "MODE_UNCONFINED",
	}, {
		Value:      UserMode,
		Name:       "user",
		GoName:     "UserMode",
		KernelName: "PACKED_MODE_USER",
		ParserName: "MODE_PROMPT",
	},
}
