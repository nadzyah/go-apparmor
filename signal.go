// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package apparmor

import (
	"fmt"

	"gitlab.com/zygoon/go-apparmor/internal"
)

// SignalNumber identifies a specific UNIX signal.
//
// Signals are divided into two classes: regular signals - each with an unique name and purpose
// and real-time signals that have no pre-determined function and have generic names with index
// in the name.
type SignalNumber uint32

// Known values of SignalNumber, as of Linux 6.8.
const (
	HangUpSignal                  SignalNumber = 1  // SIGHUP
	InterruptSignal               SignalNumber = 2  // SIGINT
	QuitSignal                    SignalNumber = 3  // SIGQUIT
	IllegalInstructionSignal      SignalNumber = 4  // SIGILL
	TrapSignal                    SignalNumber = 5  // SIGTRAP
	AbortSignal                   SignalNumber = 6  // SIGABRT
	BusErrorSignal                SignalNumber = 7  // SIGBUS
	FloatingPointExceptionSignal  SignalNumber = 8  // SIGFPE
	KillSignal                    SignalNumber = 9  // SIGKILL
	UserDefinedSignalOne          SignalNumber = 10 // SIGUSR1
	SegmentationViolationSignal   SignalNumber = 11 // SIGSEGV
	UserDefinedSignalTwo          SignalNumber = 12 // SIGUSR2
	BrokenPipeSignal              SignalNumber = 13 // SIGPIPE
	AlarmSignal                   SignalNumber = 14 // SIGALRM
	TerminateSignal               SignalNumber = 15 // SIGTERM
	StackFaultOnCoprocessorSignal SignalNumber = 16 // SIGSTKFLT
	ChildStatusSignal             SignalNumber = 17 // SIGCHLD
	ContinueSignal                SignalNumber = 18 // SIGCONT
	StopSignal                    SignalNumber = 19 // SIGSTOP
	KeyboardStopSignal            SignalNumber = 20 // SIGTSTP: Keyboard stop
	BackgroundTerminalReadSignal  SignalNumber = 21 // SIGTTIN: Background read from control terminal
	BackgroundTerminalWriteSignal SignalNumber = 22 // SIGTTOU: Background write to control terminal
	UrgentDataAvailableSignal     SignalNumber = 23 // SIGURG
	CpuLimitExceededSignal        SignalNumber = 24 // SIGXCPU
	FileSizeLimitExceededSignal   SignalNumber = 25 // SIGXFSZ
	VirtualTimeAlarmSignal        SignalNumber = 26 // SIGVTALRM
	ProfilerTimerSignal           SignalNumber = 27 // SIGPROF
	WindowSizeChangedSignal       SignalNumber = 28 // SIGWINCH
	IoSignal                      SignalNumber = 29 // SIGIO
	PowerFailureSignal            SignalNumber = 30 // SIGPWR
	SystemCallErrorSignal         SignalNumber = 31 // SIGSYS
	EmulatorTrapSignal            SignalNumber = 32 // SIGEMT
	LostSignal                    SignalNumber = 33 // SIGLOST
	UnusedSignal                  SignalNumber = 34 // SIGUNUSED
	ExistsSignal                  SignalNumber = 35 // Not a real signal (process existence test).
)

const (
	realTimeSignalBase  = 128 // Number of the first real-time signal as encoded by AppArmor.
	realTimeSignalCount = 32  // Total number of real-time signals as allowed by AppArmor.
)

// RealTimeSignal returns the AppArmor signal number for the Nth real-time signal.
func RealTimeSignal(n int) SignalNumber {
	return SignalNumber(realTimeSignalBase + n)
}

func (v SignalNumber) realTimeNumber() (rt int, ok bool) {
	if v >= realTimeSignalBase {
		if rt := int(v - realTimeSignalBase); rt <= realTimeSignalCount {
			return rt, true
		}
	}

	return 0, false
}

// String returns the human-readable name of the signal.
func (v SignalNumber) String() string {
	if rt, ok := v.realTimeNumber(); ok {
		return fmt.Sprintf("rtmin+%d", rt)
	}

	return signalNumberValues.String(v)
}

// GoString returns the corresponding Go constant or literal.
func (v SignalNumber) GoString() string {
	if rt, ok := v.realTimeNumber(); ok {
		return fmt.Sprintf("RealTimeSignal(%d)", rt)
	}

	return signalNumberValues.GoString(v)
}

// KernelString returns the corresponding Linux kernel constant or literal.
func (v SignalNumber) KernelString() string {
	if rt, ok := v.realTimeNumber(); ok {
		return fmt.Sprintf("SIGRTMIN+%d", rt)
	}

	return signalNumberValues.KernelString(v)
}

// ParserString returns the corresponding AppArmor parser constant or literal.
func (v SignalNumber) ParserString() string {
	if rt, ok := v.realTimeNumber(); ok {
		return fmt.Sprintf("MINRT_SIG+%d", rt)
	}

	return signalNumberValues.ParserString(v)
}

// signalNumberValues contains known values for SignalNumber.
var signalNumberValues = internal.EnumValues[SignalNumber]{
	{
		Value:      HangUpSignal,
		GoName:     "HangUpSignal",
		KernelName: "SIGHUP",
		ParserName: "SIGHUP",
		Name:       "hup",
	}, {
		Value:      InterruptSignal,
		GoName:     "InterruptSignal",
		KernelName: "SIGINT",
		ParserName: "SIGINT",
		Name:       "int",
	}, {
		Value:      QuitSignal,
		GoName:     "QuitSignal",
		KernelName: "SIGQUIT",
		ParserName: "SIGQUIT",
		Name:       "quit",
	}, {
		Value:      IllegalInstructionSignal,
		GoName:     "IllegalInstructionSignal",
		KernelName: "SIGILL",
		ParserName: "SIGILL",
		Name:       "ill",
	}, {
		Value:      TrapSignal,
		GoName:     "TrapSignal",
		KernelName: "SIGTRAP",
		ParserName: "SIGTRAP",
		Name:       "trap",
	}, {
		Value:      AbortSignal,
		GoName:     "AbortSignal",
		KernelName: "SIGABRT",
		ParserName: "SIGABRT",
		Name:       "abrt",
	}, {
		Value:      BusErrorSignal,
		GoName:     "BusErrorSignal",
		KernelName: "SIGBUS",
		ParserName: "SIGBUS",
		Name:       "bus",
	}, {
		Value:      FloatingPointExceptionSignal,
		GoName:     "FloatingPointExceptionSignal",
		KernelName: "SIGFPE",
		ParserName: "SIGFPE",
		Name:       "fpe",
	}, {
		Value:      KillSignal,
		GoName:     "KillSignal",
		KernelName: "SIGKILL",
		ParserName: "SIGKILL",
		Name:       "kill",
	}, {
		Value:      UserDefinedSignalOne,
		GoName:     "UserDefinedSignalOne",
		KernelName: "SIGUSR1",
		ParserName: "SIGUSR1",
		Name:       "usr1",
	}, {
		Value:      SegmentationViolationSignal,
		GoName:     "SegmentationViolationSignal",
		KernelName: "SIGSEGV",
		ParserName: "SIGSEGV",
		Name:       "segv",
	}, {
		Value:      UserDefinedSignalTwo,
		GoName:     "UserDefinedSignalTwo",
		KernelName: "SIGUSR2",
		ParserName: "SIGUSR2",
		Name:       "usr2",
	}, {
		Value:      BrokenPipeSignal,
		GoName:     "BrokenPipeSignal",
		KernelName: "SIGPIPE",
		ParserName: "SIGPIPE",
		Name:       "pipe",
	}, {
		Value:      AlarmSignal,
		GoName:     "AlarmSignal",
		KernelName: "SIGALRM",
		ParserName: "SIGALRM",
		Name:       "alrm",
	}, {
		Value:      TerminateSignal,
		GoName:     "TerminateSignal",
		KernelName: "SIGTERM",
		ParserName: "SIGTERM",
		Name:       "term",
	}, {
		Value:      StackFaultOnCoprocessorSignal,
		GoName:     "StackFaultOnCoprocessorSignal",
		KernelName: "SIGSTKFLT",
		ParserName: "SIGSTKFLT",
		Name:       "stkflt",
	}, {
		Value:      ChildStatusSignal,
		GoName:     "ChildStatusSignal",
		KernelName: "SIGCHLD",
		ParserName: "SIGCHLD",
		Name:       "chld",
	}, {
		Value:      ContinueSignal,
		GoName:     "ContinueSignal",
		KernelName: "SIGCONT",
		ParserName: "SIGCONT",
		Name:       "cont",
	}, {
		Value:      StopSignal,
		GoName:     "StopSignal",
		KernelName: "SIGSTOP",
		ParserName: "SIGSTOP",
		Name:       "stop",
	}, {
		Value:      KeyboardStopSignal,
		GoName:     "KeyboardStopSignal",
		KernelName: "SIGTSTP",
		ParserName: "SIGTSTP",
		Name:       "stp",
	}, {
		Value:      BackgroundTerminalReadSignal,
		GoName:     "BackgroundTerminalReadSignal",
		KernelName: "SIGTTIN",
		ParserName: "SIGTTIN",
		Name:       "ttin",
	}, {
		Value:      BackgroundTerminalWriteSignal,
		GoName:     "BackgroundTerminalWriteSignal",
		KernelName: "SIGTTOU",
		ParserName: "SIGTTOU",
		Name:       "ttou",
	}, {
		Value:      UrgentDataAvailableSignal,
		GoName:     "UrgentDataAvailableSignal",
		KernelName: "SIGURG",
		ParserName: "SIGURG",
		Name:       "urg",
	}, {
		Value:      CpuLimitExceededSignal,
		GoName:     "CpuLimitExceededSignal",
		KernelName: "SIGXCPU",
		ParserName: "SIGXCPU",
		Name:       "xcpu",
	}, {
		Value:      FileSizeLimitExceededSignal,
		GoName:     "FileSizeLimitExceededSignal",
		KernelName: "SIGXFSZ",
		ParserName: "SIGXFSZ",
		Name:       "xfsz",
	}, {
		Value:      VirtualTimeAlarmSignal,
		GoName:     "VirtualTimeAlarmSignal",
		KernelName: "SIGVTALRM",
		ParserName: "SIGVTALRM",
		Name:       "vtalrm",
	}, {
		Value:      ProfilerTimerSignal,
		GoName:     "ProfilerTimerSignal",
		KernelName: "SIGPROF",
		ParserName: "SIGPROF",
		Name:       "prof",
	}, {
		Value:      WindowSizeChangedSignal,
		GoName:     "WindowSizeChangedSignal",
		KernelName: "SIGWINCH",
		ParserName: "SIGWINCH",
		Name:       "winch",
	}, {
		Value:      IoSignal,
		GoName:     "IoSignal",
		KernelName: "SIGIO",
		ParserName: "SIGIO",
		Name:       "io",
	}, {
		Value:      PowerFailureSignal,
		GoName:     "PowerFailureSignal",
		KernelName: "SIGPWR",
		ParserName: "SIGPWR",
		Name:       "pwr",
	}, {
		Value:      SystemCallErrorSignal,
		GoName:     "SystemCallErrorSignal",
		KernelName: "SIGSYS",
		ParserName: "SIGSYS",
		Name:       "sys",
	}, {
		Value:      EmulatorTrapSignal,
		GoName:     "EmulatorTrapSignal",
		KernelName: "SIGEMT",
		ParserName: "SIGEMT",
		Name:       "emt",
	}, {
		Value:      LostSignal,
		GoName:     "LostSignal",
		KernelName: "SIGLOST",
		ParserName: "SIGLOST",
		Name:       "lost",
	}, {
		Value:      UnusedSignal,
		GoName:     "UnusedSignal",
		KernelName: "SIGUNUSED",
		ParserName: "SIGUNUSED",
		Name:       "unused",
	}, {
		Value:      ExistsSignal,
		GoName:     "ExistsSignal",
		KernelName: "MAXMAPPED_SIG",
		ParserName: "MAXMAPPED_SIG",
		Name:       "exists",
	},
}
