// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package apparmor

import (
	"gitlab.com/zygoon/go-apparmor/internal"
)

// TagByte encodes information about the format of the following bytes.
type TagByte uint8

// Known constants for apparmor tag codes, as of apparmor 4.0
const (
	TagU8             TagByte = iota // AA_U8
	TagU16                           // AA_U16
	TagU32                           // AA_U32
	TagU64                           // AA_U64
	TagName                          // AA_NAME
	TagString                        // AA_STRING
	TagBlob                          // AA_BLOB
	TagStructureStart                // AA_STRUCT
	TagStructureEnd                  // AA_STRUCTEND
	TagListStart                     // AA_LIST
	TagListEnd                       // AA_LISTEND
	TagArrayStart                    // AA_ARRAY
	TagArrayEnd                      // AA_ARRAYEND
	TagOffset                        // (?) SD_OFFSET
	invalidTag        = 255
)

var tagByteValues = internal.EnumValues[TagByte]{
	{
		Value:      TagU8,
		Name:       "u8",
		KernelName: "AA_U8",
		ParserName: "SD_U8",
		GoName:     "TagU8",
	}, {
		Value:      TagU16,
		Name:       "u16",
		KernelName: "AA_U16",
		ParserName: "SD_U16",
		GoName:     "TagU16",
	}, {
		Value:      TagU32,
		Name:       "u32",
		KernelName: "AA_U32",
		ParserName: "SD_U32",
		GoName:     "TagU32",
	}, {
		Value:      TagU64,
		Name:       "u64",
		KernelName: "AA_U64",
		ParserName: "SD_U64",
		GoName:     "TagU64",
	}, {
		Value:      TagName,
		Name:       "name",
		KernelName: "AA_NAME",
		ParserName: "SD_NAME",
		GoName:     "TagName",
	}, {
		Value:      TagString,
		Name:       "string",
		KernelName: "AA_STRING",
		ParserName: "SD_STRING",
		GoName:     "TagString",
	}, {
		Value:      TagBlob,
		Name:       "blob",
		KernelName: "AA_BLOB",
		ParserName: "SD_BLOB",
		GoName:     "TagBlob",
	}, {
		Value:      TagStructureStart,
		Name:       "struct-start",
		KernelName: "AA_STRUCT",
		ParserName: "SD_STRUCT",
		GoName:     "TagStructureStart",
	}, {
		Value:      TagStructureEnd,
		Name:       "struct-end",
		KernelName: "AA_STRUCTEND",
		ParserName: "SD_STRUCTEND",
		GoName:     "TagStructureEnd",
	}, {
		Value:      TagListStart,
		Name:       "list-start",
		KernelName: "AA_LIST",
		ParserName: "SD_LIST",
		GoName:     "TagListStart",
	}, {
		Value:      TagListEnd,
		Name:       "list-end",
		KernelName: "AA_LISTEND",
		ParserName: "SD_LISTEND",
		GoName:     "TagListEnd",
	}, {
		Value:      TagArrayStart,
		Name:       "array-start",
		KernelName: "AA_ARRAY",
		ParserName: "SD_ARRAY",
		GoName:     "TagArrayStart",
	}, {
		Value:      TagArrayEnd,
		Name:       "array-end",
		KernelName: "AA_ARRAYEND",
		ParserName: "SD_ARRAYEND",
		GoName:     "TagArrayEnd",
	}, {
		Value:      TagOffset,
		Name:       "offset",
		KernelName: "AA_OFFSET(?)", // The kernel does not contain this macro.
		ParserName: "SD_OFFSET",
		GoName:     "TagOffset",
	},
}

func (m TagByte) String() string {
	return tagByteValues.String(m)
}

func (m TagByte) GoString() string {
	return tagByteValues.GoString(m)
}
