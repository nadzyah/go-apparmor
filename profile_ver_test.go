// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package apparmor_test

import (
	"errors"
	"os"
	"testing"

	"gitlab.com/zygoon/go-apparmor"
)

func TestPackedProfileVersion(t *testing.T) {
	// XXX: It seems all the profiles have the exact same packed version number.
	// We may need to get older parser to generate some additional binary
	// profiles to see any difference.
	t.Run("empty.2.13.3.abi3.bin", func(t *testing.T) {
		if v := packedProfileVersionOf(t, "testdata/empty.2.13.3.abi3.bin"); v != MustPackedProfileVersion(t, 2, 7, 2, false) {
			t.Errorf("Unexpected packed profile version: %v", v)
		}
	})

	t.Run("empty.3.0.4.abi4.bin", func(t *testing.T) {
		if v := packedProfileVersionOf(t, "testdata/empty.3.0.4.abi4.bin"); v != MustPackedProfileVersion(t, 2, 7, 2, false) {
			t.Errorf("Unexpected packed profile version: %v", v)
		}
	})

	t.Run("empty.3.0.4.native.bin", func(t *testing.T) {
		if v := packedProfileVersionOf(t, "testdata/empty.3.0.4.native.bin"); v != MustPackedProfileVersion(t, 2, 7, 2, false) {
			t.Errorf("Unexpected packed profile version: %v", v)
		}
	})

	t.Run("empty.4.0.0~beta3.native.bin", func(t *testing.T) {
		if v := packedProfileVersionOf(t, "testdata/empty.4.0.0~beta3.native.bin"); v != MustPackedProfileVersion(t, 2, 7, 2, false) {
			t.Errorf("Unexpected packed profile version: %v", v)
		}
	})

	// NOTE: Having a complain-mode profile declared using profile text is not
	// implemented using the force-complain bit. That is achieved using the
	// --Complain flag to apparmor_parser.
	t.Run("complain.4.0.0~beta3.native.bin", func(t *testing.T) {
		if v := packedProfileVersionOf(t, "testdata/complain.4.0.0~beta3.native.bin"); v != MustPackedProfileVersion(t, 2, 7, 2, false) {
			t.Errorf("Unexpected packed profile version: %v", v)
		}
	})

	t.Run("complain.4.0.0~beta3.native-complain.bin", func(t *testing.T) {
		if v := packedProfileVersionOf(t, "testdata/complain.4.0.0~beta3.native-complain.bin"); v != MustPackedProfileVersion(t, 2, 7, 2, true) {
			t.Errorf("Unexpected packed profile version: %v", v)
		}
	})
}

func MakePackedProfileVersion(policy, kernel, parser uint32, complain bool) (v apparmor.PackedProfileVersion, err error) {
	err = errors.Join(v.SetPolicyVersion(policy), v.SetKernelAbi(kernel), v.SetParserAbi(parser))

	v.SetForceComplain(complain)

	return v, err
}

func MustPackedProfileVersion(t *testing.T, policy, kernel, parser uint32, complain bool) apparmor.PackedProfileVersion {
	t.Helper()

	v, err := MakePackedProfileVersion(policy, kernel, parser, complain)
	if err != nil {
		t.Fatal(err)
	}

	return v
}

func packedProfileVersionOf(t *testing.T, path string) apparmor.PackedProfileVersion {
	t.Helper()

	f, err := os.Open(path)
	if err != nil {
		t.Fatal(err)
	}

	t.Cleanup(func() {
		_ = f.Close()
	})

	_, tag, value, err := apparmor.ReadAny(f)
	if err != nil {
		t.Fatal(err)
	}

	if tag != apparmor.TagName {
		t.Fatalf("Unexpected tag: %v", tag)
	}

	if n, ok := value.(string); !ok || n != "version" {
		t.Fatalf("Unexpected name: %v", value)
	}

	_, tag, value, err = apparmor.ReadAny(f)
	if err != nil {
		t.Fatal(err)
	}

	if tag != apparmor.TagU32 {
		t.Fatalf("Unexpected tag: %v", tag)
	}

	vu32, ok := value.(uint32)
	if !ok {
		t.Fatalf("Unexpected name: %v", value)
	}

	return apparmor.PackedProfileVersion(vu32)
}
