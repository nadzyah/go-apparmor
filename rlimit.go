// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package apparmor

import "gitlab.com/zygoon/go-apparmor/internal"

// ResourceLimitNumber identifies a specific resource limit.
type ResourceLimitNumber uint32

// Known values of ResourceLimitNumber, as of Linux 6.8. The list seems to be
// pretty fossilized with no new values added in years. The values are obtained
// from <resource.h>.
const (
	ResourceLimitCpuSeconds                  ResourceLimitNumber = 0
	ResourceLimitFileSize                    ResourceLimitNumber = 1
	ResourceLimitDataSize                    ResourceLimitNumber = 2
	ResourceLimitStackSize                   ResourceLimitNumber = 3
	ResourceLimitCoreSize                    ResourceLimitNumber = 4
	ResourceLimitResidentSetSize             ResourceLimitNumber = 5
	ResourceLimitProcessCount                ResourceLimitNumber = 6
	ResourceLimitOpenFileCount               ResourceLimitNumber = 7
	ResourceLimitLockedMemory                ResourceLimitNumber = 8
	ResourceLimitAddressSpace                ResourceLimitNumber = 9
	ResourceLimitFileLockCount               ResourceLimitNumber = 10
	ResourceLimitPendingSignalCount          ResourceLimitNumber = 11
	ResourceLimitMessageQueueBytes           ResourceLimitNumber = 12
	ResourceLimitNiceValue                   ResourceLimitNumber = 13
	ResourceLimitRealtimePriority            ResourceLimitNumber = 14
	ResourceLimitRealtimeTimeoutMicroseconds ResourceLimitNumber = 15
)

// String returns the human-readable name of the limit.
func (v ResourceLimitNumber) String() string {
	return resourceLimitNumberValues.String(v)
}

// GoString returns the corresponding Go constant or literal.
func (v ResourceLimitNumber) GoString() string {
	return resourceLimitNumberValues.GoString(v)
}

// KernelString returns the corresponding Linux kernel constant or literal.
func (v ResourceLimitNumber) KernelString() string {
	return resourceLimitNumberValues.KernelString(v)
}

// ParserString returns the corresponding AppArmor parser constant or literal.
func (v ResourceLimitNumber) ParserString() string {
	return resourceLimitNumberValues.ParserString(v)
}

// resourceLimitNumberValues contains known values for ResourceLimitIndex.
var resourceLimitNumberValues = internal.EnumValues[ResourceLimitNumber]{
	{
		Value:      ResourceLimitCpuSeconds,
		GoName:     "ResourceLimitCpuSeconds",
		KernelName: "RLIMIT_CPU",
		ParserName: "RLIMIT_CPU",
		Name:       "cpu",
	}, {
		Value:      ResourceLimitFileSize,
		GoName:     "ResourceLimitFileSize",
		KernelName: "RLIMIT_FSIZE",
		ParserName: "RLIMIT_FSIZE",
		Name:       "fsize",
	}, {
		Value:      ResourceLimitDataSize,
		GoName:     "ResourceLimitDataSize",
		KernelName: "RLIMIT_DATA",
		ParserName: "RLIMIT_DATA",
		Name:       "data",
	}, {
		Value:      ResourceLimitStackSize,
		GoName:     "ResourceLimitStackSize",
		KernelName: "RLIMIT_STACK",
		ParserName: "RLIMIT_STACK",
		Name:       "stack",
	}, {
		Value:      ResourceLimitCoreSize,
		GoName:     "ResourceLimitCoreSize",
		KernelName: "RLIMIT_CORE",
		ParserName: "RLIMIT_CORE",
		Name:       "core",
	}, {
		Value:      ResourceLimitResidentSetSize,
		GoName:     "ResourceLimitResidentSetSize",
		KernelName: "RLIMIT_RSS",
		ParserName: "RLIMIT_RSS",
		Name:       "rss",
	}, {
		Value:      ResourceLimitProcessCount,
		GoName:     "ResourceLimitProcessCount",
		KernelName: "RLIMIT_NPROC",
		ParserName: "RLIMIT_NPROC",
		Name:       "nproc",
	}, {
		Value:      ResourceLimitOpenFileCount,
		GoName:     "ResourceLimitOpenFileCount",
		KernelName: "RLIMIT_NOFILE",
		ParserName: "RLIMIT_NOFILE",
		Name:       "nofile",
	}, {
		Value:      ResourceLimitLockedMemory,
		GoName:     "ResourceLimitLockedMemory",
		KernelName: "RLIMIT_MEMLOCK",
		ParserName: "RLIMIT_MEMLOCK",
		Name:       "memlock",
	}, {
		Value:      ResourceLimitAddressSpace,
		GoName:     "ResourceLimitAddressSpace",
		KernelName: "RLIMIT_AS",
		ParserName: "RLIMIT_AS",
		Name:       "as",
	}, {
		Value:      ResourceLimitFileLockCount,
		GoName:     "ResourceLimitFileLockCount",
		KernelName: "RLIMIT_LOCKS",
		ParserName: "RLIMIT_LOCKS",
		Name:       "locks",
	}, {
		Value:      ResourceLimitPendingSignalCount,
		GoName:     "ResourceLimitPendingSignalCount",
		KernelName: "RLIMIT_SIGPENDING",
		ParserName: "RLIMIT_SIGPENDING",
		Name:       "sigpending",
	}, {
		Value:      ResourceLimitMessageQueueBytes,
		GoName:     "ResourceLimitMessageQueueBytes",
		KernelName: "RLIMIT_MSGQUEUE",
		ParserName: "RLIMIT_MSGQUEUE",
		Name:       "msgqueue",
	}, {
		Value:      ResourceLimitNiceValue,
		GoName:     "ResourceLimitNiceValue",
		KernelName: "RLIMIT_NICE",
		ParserName: "RLIMIT_NICE",
		Name:       "nice",
	}, {
		Value:      ResourceLimitRealtimePriority,
		GoName:     "ResourceLimitRealtimePriority",
		KernelName: "RLIMIT_RTPRIO",
		ParserName: "RLIMIT_RTPRIO",
		Name:       "rtprio",
	}, {
		Value:      ResourceLimitRealtimeTimeoutMicroseconds,
		GoName:     "ResourceLimitRealtimeTimeoutMicroseconds",
		KernelName: "RLIMIT_RTTIME",
		ParserName: "RLIMIT_RTTIME",
		Name:       "rttime",
	},
}
