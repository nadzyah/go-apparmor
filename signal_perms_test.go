// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package apparmor_test

import (
	"testing"

	"gitlab.com/zygoon/go-apparmor"
	"gitlab.com/zygoon/go-apparmor/internal/flagtest"
)

func TestSignalPerms(t *testing.T) {
	t.Run("Sane", func(t *testing.T) {
		flagtest.Sane(t, apparmor.SignalPermissionSetValues)
	})

	t.Run("String", func(t *testing.T) {
		if n := apparmor.MaySendSignalPermission.String(); n != "send" {
			t.Fatalf("Unexpected name: %q", n)
		}
		if n := apparmor.MayReceiveSignalPermission.String(); n != "receive" {
			t.Fatalf("Unexpected name: %q", n)
		}
		if n := (apparmor.MayReceiveSignalPermission | apparmor.MaySendSignalPermission).String(); n != "send,receive" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})

	t.Run("GoString", func(t *testing.T) {
		if n := apparmor.MaySendSignalPermission.GoString(); n != "MaySendSignalPermission" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := apparmor.MayReceiveSignalPermission.GoString(); n != "MayReceiveSignalPermission" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})

	t.Run("KernelString", func(t *testing.T) {
		if n := apparmor.MaySendSignalPermission.KernelString(); n != "AA_MAY_SEND" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := apparmor.MayReceiveSignalPermission.KernelString(); n != "AA_MAY_RECEIVE" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})

	t.Run("ParserString", func(t *testing.T) {
		if n := apparmor.MaySendSignalPermission.ParserString(); n != "AA_MAY_SEND" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := apparmor.MayReceiveSignalPermission.ParserString(); n != "AA_MAY_RECEIVE" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})
}
