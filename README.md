<!--
SPDX-License-Identifier: Apache-2.0
SPDX-FileCopyrightText: Canonical Ltd
-->
# Peeking into AppArmor, in Go

Some parts of the AppArmor implementation are relatively obscure. One of those
is the binary data interchange format between the userspace parser and the
kernel side loader. This project intends to shed some light and simplify
debugging binary profiles as well as simplify construction of tailored tools.

## References

Use the source Luke!

Most of the useful insight can be obtained by looking at the kernel cross reference
at [policy\_unpack.c](https://elixir.bootlin.com/linux/latest/source/security/apparmor/policy_unpack.c)

This link is valid as of Linux 6.8.1, but should it go out of date look at security/apparmor
and look at the userspace _entry-point_, which defines the `.load` file typically available from
`/sys/kernel/security/apparmor/.load`: [aa\_fs\_profile\_load](https://elixir.bootlin.com/linux/latest/source/security/apparmor/apparmorfs.c#L1981). By following the code from there you can trace what the kenrel does with the binary that
is written there.

## Binary Data Format

As I make progress this part will document the format of the binary profile.
Please see the `DataFormat.md` file for details

## License

Everything in this code is distributed under the Apache 2.0 license.
