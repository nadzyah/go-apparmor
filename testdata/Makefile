# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Canonical Ltd
ALL=$(wildcard *.src)
V:=$(shell apparmor_parser --version | awk '/AppArmor parser version/ { print $$4 }')
$(info AppArmor parser version: $V)
all: $(patsubst %.src,%.$V.abi4.bin,$(ALL)) $(patsubst %.src,%.$V.abi3.bin,$(ALL)) $(patsubst %.src,%.$V.native.bin,$(ALL))

OPTIONS_2.10.95 =
OPTIONS_2.12 =
OPTIONS_3.0.4 =

DEPS_3.0.4 = abi/3.0

# "native" depends on the host kernel
%.$V.native.bin: %.src $(DEPS_V$)
	$(strip apparmor_parser $(OPTIONS_$V)) \
		--skip-kernel-load \
		--skip-cache \
		--ofile $@ $<

%.$V.abi4.bin: %.src $(DEPS_V$)
	$(strip apparmor_parser $(OPTIONS_$V)) \
		--policy-features=abi/4.0 \
		--kernel-features=abi/4.0 \
		--skip-kernel-load \
		--skip-cache \
		--ofile $@ $<

%.$V.abi3.bin: %.src $(DEPS_V$)
	$(strip apparmor_parser $(OPTIONS_$V)) \
		--policy-features=abi/3.0 \
		--kernel-features=abi/3.0 \
		--skip-kernel-load \
		--skip-cache \
		--ofile $@ $<

%.$V.native-complain.bin: %.src $(DEPS_V$)
	$(strip apparmor_parser $(OPTIONS_$V)) \
		--skip-kernel-load \
		--skip-cache \
		--Complain \
		--ofile $@ $<

%.dump: %.src
	apparmor_parser	\
		--dump dfa-states \
		--dump dfa-graph \
		--skip-kernel-load \
		--skip-cache \
		--ofile /dev/null \
		$< >$@ 2>&1
