// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package apparmor

import (
	"gitlab.com/zygoon/go-apparmor/internal"
)

// SignalPermissionSet is a bit-set encoding signal-specific permissions.
type SignalPermissionSet uint32

const (
	// MaySendSignalPermission indicates that a process may send a signal.
	MaySendSignalPermission SignalPermissionSet = 1 << 1 // AA_MAY_SEND
	// MayReceiveSignalPermission indicates that a process may receive a signal.
	MayReceiveSignalPermission SignalPermissionSet = 1 << 2 // AA_MAY_RECEIVE
)

var signalPermissionSetValues = internal.FlagValues[SignalPermissionSet]{
	{
		Mask:       MaySendSignalPermission,
		Name:       "send",
		GoName:     "MaySendSignalPermission",
		KernelName: "AA_MAY_SEND",
		ParserName: "AA_MAY_SEND",
	}, {
		Mask:       MayReceiveSignalPermission,
		Name:       "receive",
		GoName:     "MayReceiveSignalPermission",
		KernelName: "AA_MAY_RECEIVE",
		ParserName: "AA_MAY_RECEIVE",
	},
}

// String returns the human-readable name of the signal permission.
func (f SignalPermissionSet) String() string {
	return signalPermissionSetValues.String(f)
}

// GoString returns the corresponding Go expression.
func (f SignalPermissionSet) GoString() string {
	return signalPermissionSetValues.GoString(f)
}

// KernelString returns the corresponding Linux kernel expression.
func (f SignalPermissionSet) KernelString() string {
	return signalPermissionSetValues.KernelString(f)
}

// ParserString returns the corresponding AppArmor parser expression.
func (f SignalPermissionSet) ParserString() string {
	return signalPermissionSetValues.ParserString(f)
}
