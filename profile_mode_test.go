// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package apparmor_test

import (
	"testing"

	"gitlab.com/zygoon/go-apparmor"
	"gitlab.com/zygoon/go-apparmor/internal/enumtest"
)

func TestPackedProfileMode(t *testing.T) {
	if n := apparmor.ComplainMode.String(); n != "complain" {
		t.Fatalf("Unexpected name: %q", n)
	}

	if n := apparmor.ComplainMode.GoString(); n != "ComplainMode" {
		t.Fatalf("Unexpected name: %q", n)
	}

	if n := apparmor.ComplainMode.KernelString(); n != "PACKED_MODE_COMPLAIN" {
		t.Fatalf("Unexpected name: %q", n)
	}

	if n := apparmor.ComplainMode.ParserString(); n != "MODE_COMPLAIN" {
		t.Fatalf("Unexpected name: %q", n)
	}

	enumtest.Sane(t, apparmor.ProfileModeValues)
}
