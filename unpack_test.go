// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package apparmor_test

import (
	"testing"

	"gitlab.com/zygoon/go-apparmor"
	"gitlab.com/zygoon/go-apparmor/internal/enumtest"
)

func TestTagBytes(t *testing.T) {
	t.Run("Sane", func(t *testing.T) {
		enumtest.Sane(t, apparmor.TagByteValues)
	})
}
