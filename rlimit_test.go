// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package apparmor_test

import (
	"testing"

	"gitlab.com/zygoon/go-apparmor"
	"gitlab.com/zygoon/go-apparmor/internal/enumtest"
)

func TestResourceLimitIndex(t *testing.T) {
	if l := len(apparmor.ResourceLimitNumberValues); l != 16 {
		t.Fatalf("Unexpected number of resource limit index values: %d", l)
	}

	enumtest.Sane(t, apparmor.ResourceLimitNumberValues)
}
