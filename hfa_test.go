// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package apparmor_test

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
	"testing"

	"gitlab.com/zygoon/go-apparmor"
)

func TestExploreFileRules(t *testing.T) {
	f, err := os.Open("testdata/file.4.0.0~beta3.abi4.bin")
	if err != nil {
		t.Fatal(err)
	}

	t.Cleanup(func() {
		_ = f.Close()
	})

	forEachBlob(t, f, func(t *testing.T, profileStartOff, off int64, blob []byte) {
		if off != 0x00054a {
			t.Skip("This are not the droids you are looking for")
		}

		var hfa apparmor.HFA

		if err := hfa.ReadBinary(bytes.NewReader(blob[apparmor.BlobPadding(off, profileStartOff):])); err != nil {
			t.Fatal(err)
		}

		t.Log("flags ", hfa.Flags)
		t.Log("default ", "length", len(hfa.DefaultTable), "content", hfa.DefaultTable)
		t.Log("base    ", "length", len(hfa.BaseTable), "content", hfa.BaseTable)
		t.Log("accept 1", "length", len(hfa.AcceptTable1), "content", hfa.AcceptTable1)
		t.Log("accept 2", "length", len(hfa.AcceptTable2), "content", hfa.AcceptTable2)
		t.Log("next    ", "length", len(hfa.NextTable), "content", hfa.NextTable)
		t.Log("check   ", "length", len(hfa.CheckTable), "content", hfa.CheckTable)
		t.Log("EC      ", "length", len(hfa.EquivalenceClassTable), "content", hfa.EquivalenceClassTable)

		for idx := range hfa.NextTable {
			if hfa.CheckTable[idx] == 0 {
				continue
			}
			s := hfa.CheckTable[idx]
			t.Logf("in state %-3d and input %q go to state %-3d (or switch to state %-3d)",
				s, rune(idx-int(hfa.BaseTable[s])), hfa.NextTable[idx], hfa.DefaultTable[s])
		}

		t.Log(hfa.QueryFileRules([]byte("/path/to/")))
		t.Log(hfa.QueryFileRules([]byte("/path/to/file")))
		t.Log(hfa.QueryFileRules([]byte("/aaa/aa/a")))
	})
}

func TestExploreSignalRules(t *testing.T) {
	f, err := os.Open("testdata/signal+send+sigint,sighup+peer-label.4.0.0~beta3.native.bin")
	if err != nil {
		t.Fatal(err)
	}

	t.Cleanup(func() {
		_ = f.Close()
	})

	forEachBlob(t, f, func(t *testing.T, profileStartOff, off int64, blob []byte) {
		if off != 0x000088 {
			t.Skip("This are not the droids you are looking for")
		}

		var hfa apparmor.HFA

		if err := hfa.ReadBinary(bytes.NewReader(blob[apparmor.BlobPadding(off, profileStartOff):])); err != nil {
			t.Fatal(err)
		}

		t.Log("flags ", hfa.Flags)
		t.Log("default ", "length", len(hfa.DefaultTable), "content", hfa.DefaultTable)
		t.Log("base    ", "length", len(hfa.BaseTable), "content", hfa.BaseTable)
		t.Log("accept 1", "length", len(hfa.AcceptTable1), "content", hfa.AcceptTable1)
		t.Log("accept 2", "length", len(hfa.AcceptTable2), "content", hfa.AcceptTable2)
		t.Log("next    ", "length", len(hfa.NextTable), "content", hfa.NextTable)
		t.Log("check   ", "length", len(hfa.CheckTable), "content", hfa.CheckTable)
		t.Log("EC      ", "length", len(hfa.EquivalenceClassTable), "content", hfa.EquivalenceClassTable)

		// Find the initial state for each mediation class.
		initialStateForClass := make(map[apparmor.MediationClass]apparmor.StateID, 32)
		classTrace := make([]apparmor.MediationClass, len(hfa.AcceptTable1))
		for idx := range hfa.NextTable {
			if s := hfa.CheckTable[idx]; s == 1 {
				nextState := hfa.NextTable[idx]
				cls := apparmor.MediationClass(idx - int(hfa.BaseTable[s]))
				initialStateForClass[cls] = nextState
				classTrace[nextState] = cls
			}
		}

		// Color each state with the mediation class. The algorithm is somewhat
		// inefficient. A queue would be better.
		for {
			more := false

			for idx := range hfa.NextTable {
				s := hfa.CheckTable[idx]
				nextState := hfa.NextTable[idx]
				if classTrace[s] == apparmor.MediationClassNone {
					continue
				}
				if classTrace[s] != classTrace[nextState] {
					classTrace[nextState] = classTrace[s]
					more = true
				}
			}

			if !more {
				break
			}
		}

		// Show stuff related to signals.
		for idx := range hfa.NextTable {
			switch s := hfa.CheckTable[idx]; s {
			case 0:
				continue
			case 1:
				cls := apparmor.MediationClass(idx - int(hfa.BaseTable[s]))
				if cls != apparmor.MediationClassSignal {
					continue
				}

				t.Logf("in state %-3d and input %#-28v go to state %-3d (or switch to state %-3d)",
					s, cls, hfa.NextTable[idx], hfa.DefaultTable[s])
			case initialStateForClass[apparmor.MediationClassSignal]:
				sig := apparmor.SignalNumber(idx - int(hfa.BaseTable[s]))
				t.Logf("in state %-3d and input %#-28v go to state %-3d (or switch to state %-3d)",
					s, sig, hfa.NextTable[idx], hfa.DefaultTable[s])
			default:
				if classTrace[s] != apparmor.MediationClassSignal {
					continue
				}

				r := rune(idx - int(hfa.BaseTable[s]))
				t.Logf("in state %-3d and input %#-28q go to state %-3d (or switch to state %-3d)",
					s, r, hfa.NextTable[idx], hfa.DefaultTable[s])
			}
		}

		t.Run("may-send-sigint-to-potato", func(t *testing.T) {
			allow, audit, quiet := hfa.QuerySignalRules(apparmor.InterruptSignal, []byte("potato"))
			t.Log("sigint", "peer=potato", "allow", allow, "audit", audit, "quiet", quiet)

			if allow != apparmor.MaySendSignalPermission || audit != 0 || quiet != 0 {
				t.Error("Unexpected permissions", allow, audit, quiet)
			}
		})

		t.Run("may-receive-sigrt+5-from-anyone", func(t *testing.T) {
			allow, audit, quiet := hfa.QuerySignalRules(apparmor.RealTimeSignal(5), []byte("some-label"))
			t.Log("sigrt+5", "peer=?", "allow", allow, "audit", audit, "quiet", quiet)

			if allow != apparmor.MayReceiveSignalPermission || audit != apparmor.MayReceiveSignalPermission || quiet != 0 {
				t.Error("Unexpected permissions", allow, audit, quiet)
			}
		})

		t.Run("cannot-receive-sigrt+6-from-anyone", func(t *testing.T) {
			allow, audit, quiet := hfa.QuerySignalRules(apparmor.RealTimeSignal(6), []byte("some-label"))
			t.Log("sigrt+6", "peer=?", "allow", allow, "audit", audit, "quiet", quiet)

			if allow != 0 || audit != 0 || quiet != 0 {
				t.Error("Unexpected permissions", allow, audit, quiet)
			}
		})

		t.Run("cannot-receive-sigrt+7-from-anyone", func(t *testing.T) {
			allow, audit, quiet := hfa.QuerySignalRules(apparmor.RealTimeSignal(7), []byte("some-label"))
			t.Log("sigrt+7", "peer=?", "allow", allow, "audit", audit, "quiet", quiet)

			if allow != 0 || audit != 0 || quiet != apparmor.MayReceiveSignalPermission {
				t.Error("Unexpected permissions", allow, audit, quiet)
			}
		})
	})
}

func TestReadTableSet(t *testing.T) {
	forEachProfileBinary(t, func(t *testing.T, f *os.File) {
		forEachBlob(t, f, func(t *testing.T, profileStartOff, off int64, blob []byte) {
			pad := apparmor.BlobPadding(off, profileStartOff)

			t.Logf("Found blob of %d bytes at offset %06x, in a profile at offset %06x",
				len(blob), off, profileStartOff)
			t.Logf("Blob has %d bytes of padding", pad)
			t.Logf("Blob data: %#v", blob)

			var hfa apparmor.HFA

			if err := hfa.ReadBinary(bytes.NewReader(blob[pad:])); err != nil {
				t.Fatal(err)
			}

			t.Log("flags", hfa.Flags)

			t.Log("default ", hfa.DefaultTable)
			t.Log("base    ", hfa.BaseTable)
			if len(hfa.BaseTable) != len(hfa.DefaultTable) {
				t.Errorf("Base and default tables have different lengths: base %d, default %d",
					len(hfa.BaseTable), len(hfa.DefaultTable))
			}

			t.Log("accept 1", hfa.AcceptTable1)
			t.Log("accept 2", hfa.AcceptTable2)
			if len(hfa.AcceptTable1) != len(hfa.AcceptTable2) {
				t.Errorf("Accept tables have different length: accept1 %d, accept2 %d",
					len(hfa.AcceptTable1), len(hfa.AcceptTable2))
			}

			if len(hfa.AcceptTable1) != len(hfa.BaseTable) {
				t.Errorf("Accept and base tables have different length: accept1 %d, base %d",
					len(hfa.AcceptTable1), len(hfa.BaseTable))
			}

			t.Log("next    ", hfa.NextTable)
			t.Log("check   ", hfa.CheckTable)
			if len(hfa.NextTable) != len(hfa.CheckTable) {
				t.Errorf("Next and check tables have different length: next %d, check %d",
					len(hfa.NextTable), len(hfa.CheckTable))
			}
		})
	})
}

func forEachBlob(t *testing.T, f *os.File, fn func(t *testing.T, profileStartOff, off int64, blob []byte)) {
	t.Helper()

	profileStartOff := int64(0)
	structureNesting := 0

	for {
		off, tag, value, err := apparmor.ReadAny(f)
		if err != nil {
			if errors.Is(err, io.EOF) {
				break
			}

			t.Fatal(err)
		}

		switch tag {
		case apparmor.TagStructureStart:
			structureNesting++
		case apparmor.TagStructureEnd:
			structureNesting--
		case apparmor.TagName:
			// When we see a NAME "version" field at structure nesting level zero,
			// then it must be the start of a new profile.
			if structureNesting == 0 && value.(string) == "version" {
				profileStartOff = off
			}
		case apparmor.TagBlob:
			off := off
			blob := value.([]byte)

			t.Run(fmt.Sprintf("blob@%06x", off), func(t *testing.T) {
				t.Helper()
				fn(t, profileStartOff, off, blob)
			})
		}
	}
}
