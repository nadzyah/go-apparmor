// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package apparmor_test

import (
	"testing"

	"gitlab.com/zygoon/go-apparmor"
	"gitlab.com/zygoon/go-apparmor/internal/enumtest"
	"gitlab.com/zygoon/go-apparmor/internal/flagtest"
)

func TestCapabilityNumber(t *testing.T) {
	if l := len(apparmor.CapabilityNumberValues); l != 41 {
		t.Fatalf("Unexpected number of Linux capabilities: %d", l)
	}

	t.Run("Sane", func(t *testing.T) {
		enumtest.Sane(t, apparmor.CapabilityNumberValues)
	})

	t.Run("String", func(t *testing.T) {
		if n := apparmor.CapabilityNetworkAdmin.String(); n != "net-admin" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})

	t.Run("GoString", func(t *testing.T) {
		if n := apparmor.CapabilityNetworkAdmin.GoString(); n != "CapabilityNetworkAdmin" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})

	t.Run("KernelString", func(t *testing.T) {
		if n := apparmor.CapabilityNetworkAdmin.KernelString(); n != "CAP_NET_ADMIN" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})

	t.Run("ParserString", func(t *testing.T) {
		if n := apparmor.CapabilityNetworkAdmin.ParserString(); n != "CAP_NET_ADMIN" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})
}

func TestCapabilitySet(t *testing.T) {
	if len(apparmor.CapabilitySetValues) != len(apparmor.CapabilityNumberValues) {
		t.Fatalf("Number of capabilities is different from number of capability bit-masks?")
	}

	t.Run("Sane", func(t *testing.T) {
		flagtest.Sane(t, apparmor.CapabilitySetValues)
	})

	mask := apparmor.CapabilityNetworkAdmin.Mask() | apparmor.CapabilityNetworkBroadcast.Mask()

	t.Run("String", func(t *testing.T) {
		if n := mask.String(); n != "net-broadcast,net-admin" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})

	t.Run("GoString", func(t *testing.T) {
		if n := mask.GoString(); n != "1<<CapabilityNetworkBroadcast|1<<CapabilityNetworkAdmin" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})

	t.Run("KernelString", func(t *testing.T) {
		if n := mask.KernelString(); n != "(1<<CAP_NET_BROADCAST)|(1<<CAP_NET_ADMIN)" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})

	t.Run("ParserString", func(t *testing.T) {
		if n := mask.ParserString(); n != "(1<<CAP_NET_BROADCAST)|(1<<CAP_NET_ADMIN)" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})
}
