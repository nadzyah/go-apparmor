// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package apparmor

import "gitlab.com/zygoon/go-apparmor/internal"

// MediationClass identifies the class of object being mediated.
//
// Coupled to each class is a type with a set of possible permissions. Some
// classes are not implemented in the kernel and the parser. The "file" class is
// special in that it does not exist in the policy database, but instead is
// stored in a separate HFA unique to file rules.
type MediationClass uint8

const (
	MediationClassNone            MediationClass = 0
	MediationClassUnknown         MediationClass = 1
	MediationClassFile            MediationClass = 2
	MediationClassCapability      MediationClass = 3
	MediationClassOldNetwork      MediationClass = 4
	MediationClassResourceLimits  MediationClass = 5
	MediationClassDomain          MediationClass = 6 // ???
	MediationClassMount           MediationClass = 7
	MediationClassNamespaceDomain MediationClass = 8
	MediationClassProcessTrace    MediationClass = 9
	MediationClassSignal          MediationClass = 10
	MediationClassExtendedMatch   MediationClass = 11
	MediationClassEnvironment     MediationClass = 12
	MediationClassArgumentVector  MediationClass = 13
	MediationClassNetwork         MediationClass = 14
	// 15 is unused
	MediationClassLabel             MediationClass = 16
	MediationClassPosixMessageQueue MediationClass = 17
	MediationClassSysVMessageQueue  MediationClass = 18
	MediationClassModule            MediationClass = 19
	MediationClassDisplayLSM        MediationClass = 20
	MediationClassNamespace         MediationClass = 21
	MediationClassIOUring           MediationClass = 22
	MediationClassX                 MediationClass = 31
	MediationClassDBus              MediationClass = 32
)

// String returns the human-readable name of the mediation class.
func (v MediationClass) String() string {
	return mediationClassValues.String(v)
}

// GoString returns the corresponding Go constant or literal.
func (v MediationClass) GoString() string {
	return mediationClassValues.GoString(v)
}

// KernelString returns the corresponding Linux kernel constant or literal.
func (v MediationClass) KernelString() string {
	return mediationClassValues.KernelString(v)
}

// ParserString returns the corresponding AppArmor parser constant or literal.
func (v MediationClass) ParserString() string {
	return mediationClassValues.ParserString(v)
}

var mediationClassValues = internal.EnumValues[MediationClass]{
	{
		Value:      MediationClassNone,
		Name:       "none",
		GoName:     "MediationClassNone",
		KernelName: "AA_CLASS_NONE",
		ParserName: "AA_CLASS_COND", // ??
	}, {
		Value:      MediationClassUnknown,
		Name:       "unknown",
		GoName:     "MediationClassUnknown",
		KernelName: "AA_CLASS_UNKNOWN",
		ParserName: "AA_CLASS_UNKNOWN",
	}, {
		Value:      MediationClassFile,
		Name:       "file",
		GoName:     "MediationClassFile",
		KernelName: "AA_CLASS_FILE",
		ParserName: "AA_CLASS_FILE",
	}, {
		Value:      MediationClassCapability,
		Name:       "capability",
		GoName:     "MediationClassCapability",
		KernelName: "AA_CLASS_CAP",
		ParserName: "AA_CLASS_CAP",
	}, {
		Value:      MediationClassOldNetwork,
		Name:       "old-network",
		GoName:     "MediationClassOldNetwork",
		KernelName: "AA_CLASS_DEPRECATED",
		ParserName: "AA_CLASS_NET",
	}, {
		Value:      MediationClassResourceLimits,
		Name:       "rlimit",
		GoName:     "MediationClassResourceLimits",
		KernelName: "AA_CLASS_RLIMITS",
		ParserName: "AA_CLASS_RLIMITS",
	}, {
		Value:      MediationClassDomain,
		Name:       "domain",
		GoName:     "MediationClassDomain",
		KernelName: "AA_CLASS_DOMAIN",
		ParserName: "AA_CLASS_DOMAIN",
	}, {
		Value:      MediationClassMount,
		Name:       "mount",
		GoName:     "MediationClassMount",
		KernelName: "AA_CLASS_MOUNT",
		ParserName: "AA_CLASS_MOUNT",
	}, {
		Value:      MediationClassNamespaceDomain,
		Name:       "ns-domain",
		GoName:     "MediationClassNamespaceDomain",
		KernelName: "(missing) AA_CLASS_NS_DOMAIN", // Kernel does not have this value.
		ParserName: "AA_CLASS_NS_DOMAIN",
	}, {
		Value:      MediationClassProcessTrace,
		Name:       "ptrace",
		GoName:     "MediationClassProcessTrace",
		KernelName: "AA_CLASS_PTRACE",
		ParserName: "AA_CLASS_PTRACE",
	}, {
		Value:      MediationClassSignal,
		Name:       "signal",
		GoName:     "MediationClassSignal",
		KernelName: "AA_CLASS_SIGNAL",
		ParserName: "AA_CLASS_SIGNAL",
	}, {
		Value:      MediationClassExtendedMatch,
		Name:       "xmatch",
		GoName:     "MediationClassExtendedMatch",
		KernelName: "AA_CLASS_XMATCH",
		ParserName: "AA_CLASS_XMATCH",
	}, {
		Value:      MediationClassEnvironment,
		Name:       "env",
		GoName:     "MediationClassEnvironment",
		KernelName: "AA_CLASS_ENV",
		ParserName: "(missing) AA_CLASS_ENV", // Parser does not have this value.
	}, {
		Value:      MediationClassArgumentVector,
		Name:       "argv",
		GoName:     "MediationClassArgumentVector",
		KernelName: "AA_CLASS_ARGV",
		ParserName: "(missing) AA_CLASS_ARGV", // Parser does not have this value.
	}, {
		Value:      MediationClassNetwork,
		Name:       "network",
		GoName:     "MediationClassNetwork",
		KernelName: "AA_CLASS_NET",
		ParserName: "AA_CLASS_NETV8",
	}, {
		Value:      MediationClassLabel,
		Name:       "label",
		GoName:     "MediationClassLabel",
		KernelName: "AA_CLASS_LABEL",
		ParserName: "AA_CLASS_LABEL",
	}, {
		Value:      MediationClassPosixMessageQueue,
		Name:       "posix-mqueue",
		GoName:     "MediationClassPosixMessageQueue",
		KernelName: "AA_CLASS_POSIX_MQUEUE",
		ParserName: "AA_CLASS_POSIX_MQUEUE",
	}, {
		Value:      MediationClassSysVMessageQueue,
		Name:       "sysv-mqueue",
		GoName:     "MediationClassSysVMessageQueue",
		KernelName: "", // Kernel does not have this value.
		ParserName: "AA_CLASS_SYSV_MQUEUE",
	}, {
		Value:      MediationClassModule,
		Name:       "module",
		GoName:     "MediationClassModule",
		KernelName: "AA_CLASS_MODULE",
		ParserName: "AA_CLASS_MODULE",
	}, {
		Value:      MediationClassDisplayLSM,
		Name:       "display-lsm",
		GoName:     "MediationClassDisplayLSM",
		KernelName: "AA_CLASS_DISPLAY_LSM",
		ParserName: "AA_CLASS_DISPLAY_LSM",
	}, {
		Value:      MediationClassNamespace,
		Name:       "namespace",
		GoName:     "MediationClassNamespace",
		KernelName: "AA_CLASS_NS",
		ParserName: "AA_CLASS_NS",
	}, {
		Value:      MediationClassIOUring,
		Name:       "io-uring",
		GoName:     "MediationClassIOUring",
		KernelName: "AA_CLASS_IO_URING",
		ParserName: "AA_CLASS_IO_URING",
	}, {
		Value:      MediationClassX,
		Name:       "x",
		GoName:     "MediationClassX",
		KernelName: "AA_CLASS_X",
		ParserName: "AA_CLASS_X",
	}, {
		Value:      MediationClassDBus,
		Name:       "dbus",
		GoName:     "MediationClassDBus",
		KernelName: "AA_CLASS_DBUS", // Commented out
		ParserName: "AA_CLASS_DBUS",
	},
}
