// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package apparmor

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"io"

	"gitlab.com/zygoon/go-apparmor/internal"
)

// BlobPadding returns the amount of leading padding in a blob.
//
// The two offset values are for the tag byte of the blob and for the tag byte
// of the "version" name in the profile - the first field.
//
// The returned number of bytes encode leading zeros in any blob. The
// computation is more complex because blob content is aligned to 8 bytes
// counted from the start of the profile, which does not need to be aligned to
// 8 bytes itself.
func BlobPadding(blobOffset, profileOffset int64) (pad int) {
	blobOffset -= profileOffset
	blobOffset += 1 // tag byte for the blob
	blobOffset += 4 // length of the blob

	return int(alignTo8(blobOffset))
}

// alignTo8 returns the number of bytes needed to complete alignment to an
// 8-byte boundary, for some content of length n.
func alignTo8[T interface {
	~uint | ~uint8 | uint16 | uint32 | uint64 | ~int | ~int8 | ~int16 | ~int32 | ~int64
}](n T) T {
	if r := n % 8; r != 0 {
		return 8 - r
	}

	return 0
}

// StateID is a number representing a state in a HFA.
type StateID uint16

// HFA is a Hybrid Finite Automation.
//
// HFA is a variation of Deterministic Finite Automaton, representing a directed
// graph where individual states have a set of accept values. A single HFA may
// be used to encode rules governing different mediation classes, so both accept
// tables which encode the effective permissions are stored as untyped uint32
// pairs.
type HFA struct {
	Flags                 StateMachineFlags
	Version               string    // Always "notflex".
	Name                  string    // Always empty.
	AcceptTable1          []uint32  // indexed by state
	AcceptTable2          []uint32  // indexed by state
	BaseTable             []int32   // indexed by state
	DefaultTable          []StateID // indexed by state
	NextTable             []StateID // indexed by state + input + base[state]
	CheckTable            []StateID // indexed by state + input + base[state]
	EquivalenceClassTable []byte    // indexed by input (TODO: verify this)
}

func (hfa *HFA) query(input []byte) (uint32, uint32) {
	var s StateID = 1

	for _, c := range input {
		if biasedC := int(hfa.BaseTable[s]) + int(c); hfa.CheckTable[biasedC] == s {
			s = hfa.NextTable[biasedC]
		} else {
			s = hfa.DefaultTable[s]
		}
	}

	return hfa.AcceptTable1[s], hfa.AcceptTable2[s]
}

func (hfa *HFA) queryEx(input0, input1 byte, inputRest []byte) (uint32, uint32) {
	var s StateID = 1

	if biasedC := int(hfa.BaseTable[s]) + int(input0); hfa.CheckTable[biasedC] == s {
		s = hfa.NextTable[biasedC]
	} else {
		s = hfa.DefaultTable[s]
	}

	if biasedC := int(hfa.BaseTable[s]) + int(input1); hfa.CheckTable[biasedC] == s {
		s = hfa.NextTable[biasedC]
	} else {
		s = hfa.DefaultTable[s]
	}

	for _, c := range inputRest {
		if biasedC := int(hfa.BaseTable[s]) + int(c); hfa.CheckTable[biasedC] == s {
			s = hfa.NextTable[biasedC]
		} else {
			s = hfa.DefaultTable[s]
		}
	}

	return hfa.AcceptTable1[s], hfa.AcceptTable2[s]
}

// TODO: add file permission type
func (hfa *HFA) QueryFileRules(path []byte) (uint32, uint32) {
	// File rules are not using mediation classes but instead have an entirely
	// separate policy database specific to file rules.
	return hfa.query(path)
}

// QuerySignalRules performs a query for signal mediation rules.
//
// The query is specific to a signal number and the apparmor security label of
// the peer process (sender or receiver of the signal being queried).
//
// The return values aer the set of allowed operations, the set of denied
// operations that emit an audit message and the set of denied operations that
// do not emit an audit message.
func (hfa *HFA) QuerySignalRules(signal SignalNumber, peerLabel []byte) (allow, audit, quiet SignalPermissionSet) {
	a1, a2 := hfa.queryEx(byte(MediationClassSignal), byte(signal), peerLabel)

	allow = SignalPermissionSet(a1)
	audit = SignalPermissionSet(a2 & 0x1fc07f)
	quiet = SignalPermissionSet((a2 >> 7) & 0x1fc07f)

	return allow, audit, quiet
}

// ReadBinary reads the HFA from the given reader.
//
// The reader must point to the first byte of the magic number identifying the
// table set header. A typical blob is not aligned. Use BlobPadding() to
// compute the number of bytes to discard.
func (hfa *HFA) ReadBinary(r io.Reader) error {
	var setHeader tableSetHeader

	if err := binary.Read(r, binary.BigEndian, &setHeader); err != nil {
		return fmt.Errorf("cannot read table set header: %w", err)
	}

	if setHeader.Magic != headerMagic {
		return fmt.Errorf("unexpected magic value of table set: %#x", setHeader.Magic)
	}

	hfa.Flags = setHeader.Flags

	version, name, err := setHeader.readVersionName(r)
	if err != nil {
		return fmt.Errorf("cannot read table set version and name: %w", err)
	}

	hfa.Version = version
	hfa.Name = name

	if err := setHeader.readPadding(r); err != nil {
		return fmt.Errorf("cannot discard padding after table set header: %w", err)
	}

	for {
		var header tableHeader

		if err := binary.Read(r, binary.BigEndian, &header); err != nil {
			// There's no up-front counter that tells us how many tables to expect.
			if errors.Is(err, io.EOF) {
				break
			}

			return fmt.Errorf("cannot read table header: %w", err)
		}

		if header.LowDimensionLength == 0 {
			return fmt.Errorf("unexpected empty table %v", header.ID)
		}

		if header.HighDimensionLength != 0 {
			return fmt.Errorf("unexpected two-dimensional table %v", header.ID)
		}

		var err error

		switch header.ID {
		case acceptTable1ID:
			hfa.AcceptTable1 = make([]uint32, header.LowDimensionLength)
			err = binary.Read(r, binary.BigEndian, hfa.AcceptTable1)
		case acceptTable2ID:
			hfa.AcceptTable2 = make([]uint32, header.LowDimensionLength)
			err = binary.Read(r, binary.BigEndian, hfa.AcceptTable2)
		case baseTableID:
			hfa.BaseTable = make([]int32, header.LowDimensionLength)
			err = binary.Read(r, binary.BigEndian, hfa.BaseTable)
		case defaultTableID:
			hfa.DefaultTable = make([]StateID, header.LowDimensionLength)
			err = binary.Read(r, binary.BigEndian, hfa.DefaultTable)
		case equivalenceClassTableID:
			hfa.EquivalenceClassTable = make([]byte, header.LowDimensionLength)
			err = binary.Read(r, binary.BigEndian, hfa.EquivalenceClassTable)
		case nextTableID:
			hfa.NextTable = make([]StateID, header.LowDimensionLength)
			err = binary.Read(r, binary.BigEndian, hfa.NextTable)
		case checkTableID:
			hfa.CheckTable = make([]StateID, header.LowDimensionLength)
			err = binary.Read(r, binary.BigEndian, hfa.CheckTable)
		default:
			err = fmt.Errorf("unexpected table ID: %v", header.ID)
		}

		if err != nil {
			return fmt.Errorf("cannot read data of table %v: %w", header.ID, err)
		}

		if err := header.readPadding(r); err != nil {
			return fmt.Errorf("cannot discard padding after table %v: %w", header.ID, err)
		}
	}

	return nil
}

const headerMagic = 0x1B5E783D

// tableSetHeader is a header that describes a set of tables and their data.
type tableSetHeader struct {
	Magic      uint32            // Always 0x1B5E783D (headerMagic).
	HeaderSize uint32            // Size of the header, version and name fields.
	SetSize    uint32            // Size of the entire data set.
	Flags      StateMachineFlags // Flags governing all the tables in this set.
}

func (h *tableSetHeader) readVersionName(r io.Reader) (version, name string, err error) {
	versionNameBlob := make([]byte, (h.HeaderSize)-4-4-4-2)
	if _, err := io.ReadFull(r, versionNameBlob); err != nil {
		return "", "", err
	}

	versionBlob, nameBlob, ok := bytes.Cut(versionNameBlob, []byte{0, 0})
	if !ok {
		return "", "", fmt.Errorf("unexpected format of table set version and name: %q", versionNameBlob)
	}

	// versionBlob suffix is removed by using []byte{0, 0} as the separator.
	nameBlob = bytes.TrimSuffix(nameBlob, []byte{0})
	version = string(versionBlob)
	name = string(nameBlob)

	return version, name, nil
}

func (h *tableSetHeader) readPadding(r io.Reader) error {
	_, err := io.CopyN(io.Discard, r, int64(alignTo8(h.HeaderSize)))

	return err
}

// tableHeader is a fixed-size header describing the data that follows in the data-set.
type tableHeader struct {
	ID                  tableID
	ItemSize            tableItemSize
	HighDimensionLength uint32
	LowDimensionLength  uint32
}

func (h *tableHeader) readPadding(r io.Reader) error {
	// 2 + 2 + 4 + 4 is the size of tableHeader
	_, err := io.CopyN(io.Discard, r,
		int64(alignTo8(2+2+4+4+uint32(h.ItemSize)*uint32(h.LowDimensionLength))))

	return err
}

// StateMachineFlags encodes flags affecting the entire table set.
type StateMachineFlags uint16

// Known values of StateMachineFlags
const (
	// FlagDiffEncode indicates that differential encoding used.
	FlagDiffEncode StateMachineFlags = 1
	// FlagOutOfBandTransition indicates that one out of band transition is possible.
	FlagOutOfBandTransition StateMachineFlags = 2
)

// String returns the human-readable name of the flags.
func (v StateMachineFlags) String() string {
	return tableSetHeaderFlagsValues.String(v)
}

// GoString returns the corresponding Go constant or literal.
func (v StateMachineFlags) GoString() string {
	return tableSetHeaderFlagsValues.GoString(v)
}

// KernelString returns the corresponding Linux kernel constant or literal.
func (v StateMachineFlags) KernelString() string {
	return tableSetHeaderFlagsValues.KernelString(v)
}

// ParserString returns the corresponding AppArmor parser constant or literal.
func (v StateMachineFlags) ParserString() string {
	return tableSetHeaderFlagsValues.ParserString(v)
}

var tableSetHeaderFlagsValues = internal.FlagValues[StateMachineFlags]{{
	Mask:       FlagDiffEncode,
	Name:       "diff-encoding",
	GoName:     "FlagDiffEncode",
	KernelName: "YYTH_FLAG_DIFF_ENCODE",
	ParserName: "YYTH_FLAG_DIFF_ENCODE",
}, {
	Mask:       FlagOutOfBandTransition,
	Name:       "out-of-band-transition",
	GoName:     "FlagOutOfBandTransition",
	KernelName: "YYTH_FLAG_OOB_TRANS",
	ParserName: "YYTH_FLAG_OOB_TRANS",
}}

// tableID is an identifier of a particular class of table inside a table set.
type tableID uint16

// Known values for tableID. Note that the values do not start at zero.
// Kernel automatically subtracts one from the table ID for convenience.
// Here we stay true to what is in the actual binary files.
const (
	acceptTable1ID          tableID = 1
	baseTableID             tableID = 2
	checkTableID            tableID = 3
	defaultTableID          tableID = 4
	equivalenceClassTableID tableID = 5
	metaTableID             tableID = 6 // ??? - unused
	acceptTable2ID          tableID = 7
	nextTableID             tableID = 8
)

// String returns the human-readable name of the table ID.
func (v tableID) String() string {
	return tableIDValues.String(v)
}

// GoString returns the corresponding Go constant or literal.
func (v tableID) GoString() string {
	return tableIDValues.GoString(v)
}

// KernelString returns the corresponding Linux kernel constant or literal.
func (v tableID) KernelString() string {
	return tableIDValues.KernelString(v)
}

// ParserString returns the corresponding AppArmor parser constant or literal.
func (v tableID) ParserString() string {
	return tableIDValues.ParserString(v)
}

var tableIDValues = internal.EnumValues[tableID]{
	{
		Value:      acceptTable1ID,
		Name:       "accept",
		GoName:     "acceptTable1ID",
		KernelName: "YYTD_ID_ACCEPT",
		ParserName: "YYTD_ID_ACCEPT",
	}, {
		Value:      baseTableID,
		Name:       "base",
		GoName:     "baseTableID",
		KernelName: "YYTD_ID_BASE",
		ParserName: "YYTD_ID_BASE",
	}, {
		Value:      checkTableID,
		Name:       "check",
		GoName:     "checkTableID",
		KernelName: "YYTD_ID_CHK",
		ParserName: "YYTD_ID_CHK",
	}, {
		Value:      defaultTableID,
		Name:       "default",
		GoName:     "defaultTableID",
		KernelName: "YYTD_ID_DEF",
		ParserName: "YYTD_ID_DEF",
	}, {
		Value:      equivalenceClassTableID,
		Name:       "equivalence class",
		GoName:     "equivalenceClassTableID",
		KernelName: "YYTD_ID_EC",
		ParserName: "YYTD_ID_EC",
	}, {
		Value:      metaTableID,
		Name:       "meta",
		GoName:     "metaTableID",
		KernelName: "YYTD_ID_META",
		ParserName: "YYTD_ID_META",
	}, {
		Value:      acceptTable2ID,
		Name:       "accept2",
		GoName:     "acceptTable2ID",
		KernelName: "YYTD_ID_ACCEPT2",
		ParserName: "YYTD_ID_ACCEPT2",
	}, {
		Value:      nextTableID,
		Name:       "next",
		GoName:     "nextTableID",
		KernelName: "YYTD_ID_NXT",
		ParserName: "YYTD_ID_NXT",
	},
}

// tableItemSize encodes the size of an element in bytes.
type tableItemSize uint16

// Known values for tableItemSize.
const (
	data8  tableItemSize = 1
	data16 tableItemSize = 2
	data32 tableItemSize = 4
	data64 tableItemSize = 8
)

// String returns the human-readable name of the element size.
func (v tableItemSize) String() string {
	return tableItemSizeValues.String(v)
}

// GoString returns the corresponding Go constant or literal.
func (v tableItemSize) GoString() string {
	return tableItemSizeValues.GoString(v)
}

// KernelString returns the corresponding Linux kernel constant or literal.
func (v tableItemSize) KernelString() string {
	return tableItemSizeValues.KernelString(v)
}

// ParserString returns the corresponding AppArmor parser constant or literal.
func (v tableItemSize) ParserString() string {
	return tableItemSizeValues.ParserString(v)
}

var tableItemSizeValues = internal.EnumValues[tableItemSize]{
	{
		Value:      data8,
		Name:       "data8",
		GoName:     "data8",
		KernelName: "YYTD_DATA8",
		ParserName: "YYTD_DATA8",
	}, {
		Value:      data16,
		Name:       "data16",
		GoName:     "data16",
		KernelName: "YYTD_DATA16",
		ParserName: "YYTD_DATA16",
	}, {
		Value:      data32,
		Name:       "data32",
		GoName:     "data32",
		KernelName: "YYTD_DATA32",
		ParserName: "YYTD_DATA32",
	}, {
		Value:      data64,
		Name:       "data64",
		GoName:     "data64",
		KernelName: "YYTD_DATA64",
		ParserName: "YYTD_DATA64",
	},
}
