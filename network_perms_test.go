// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package apparmor_test

import (
	"testing"

	"gitlab.com/zygoon/go-apparmor"
	"gitlab.com/zygoon/go-apparmor/internal/flagtest"
)

func TestNetworkPermissionSet(t *testing.T) {
	t.Run("Sane", func(t *testing.T) {
		flagtest.Sane(t, apparmor.NetworkPermissionSetValues)
	})

	t.Run("String", func(t *testing.T) {
		if n := apparmor.NetworkSendPermission.String(); n != "send" {
			t.Fatalf("Unexpected name: %q", n)
		}
		if n := apparmor.NetworkReceivePermission.String(); n != "receive" {
			t.Fatalf("Unexpected name: %q", n)
		}
		if n := (apparmor.NetworkSendPermission | apparmor.NetworkReceivePermission).String(); n != "send,receive" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})

	t.Run("GoString", func(t *testing.T) {
		if n := apparmor.NetworkSendPermission.GoString(); n != "NetworkSendPermission" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := apparmor.NetworkReceivePermission.GoString(); n != "NetworkReceivePermission" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})

	t.Run("KernelString", func(t *testing.T) {
		if n := apparmor.NetworkSendPermission.KernelString(); n != "AA_NET_SEND" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := apparmor.NetworkReceivePermission.KernelString(); n != "AA_NET_RECEIVE" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})

	t.Run("ParserString", func(t *testing.T) {
		if n := apparmor.NetworkSendPermission.ParserString(); n != "AA_NET_SEND" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := apparmor.NetworkReceivePermission.ParserString(); n != "AA_NET_RECEIVE" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})
}

func TestNetworkPermissionExtendedSet(t *testing.T) {
	t.Run("Sane", func(t *testing.T) {
		flagtest.Sane(t, apparmor.NetworkPermissionExtendedSetValues)
	})

	t.Run("String", func(t *testing.T) {
		if n := apparmor.NetworkPermissionExtendedSet(apparmor.NetworkSendPermission).String(); n != "send" {
			t.Fatalf("Unexpected name: %q", n)
		}
		if n := apparmor.NetworkAcceptPermission.String(); n != "accept" {
			t.Fatalf("Unexpected name: %q", n)
		}
		if n := (apparmor.NetworkPermissionExtendedSet(apparmor.NetworkSendPermission) | apparmor.NetworkAcceptPermission).String(); n != "send,accept" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})

	t.Run("GoString", func(t *testing.T) {
		if n := apparmor.NetworkPermissionExtendedSet(apparmor.NetworkSendPermission).GoString(); n != "NetworkSendPermission" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := apparmor.NetworkAcceptPermission.GoString(); n != "NetworkAcceptPermission" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})

	t.Run("KernelString", func(t *testing.T) {
		if n := apparmor.NetworkPermissionExtendedSet(apparmor.NetworkSendPermission).KernelString(); n != "AA_NET_SEND" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := apparmor.NetworkAcceptPermission.KernelString(); n != "AA_NET_ACCEPT" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})

	t.Run("ParserString", func(t *testing.T) {
		if n := apparmor.NetworkPermissionExtendedSet(apparmor.NetworkSendPermission).ParserString(); n != "AA_NET_SEND" {
			t.Fatalf("Unexpected name: %q", n)
		}

		if n := apparmor.NetworkAcceptPermission.ParserString(); n != "AA_NET_ACCEPT" {
			t.Fatalf("Unexpected name: %q", n)
		}
	})
}
