// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package apparmor

var (
	ProfileModeValues                  = profileModeValues
	ResourceLimitNumberValues          = resourceLimitNumberValues
	SignalNumberValues                 = signalNumberValues
	SignalPermissionSetValues          = signalPermissionSetValues
	CapabilityNumberValues             = capabilityNumberValues
	CapabilitySetValues                = capabilitySetValues
	TagByteValues                      = tagByteValues
	NetworkPermissionSetValues         = networkPermissionSetValues
	NetworkPermissionExtendedSetValues = networkPermissionExtendedSetValues
	MediationClassValues               = mediationClassValues
)
