// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package apparmor

import "gitlab.com/zygoon/go-apparmor/internal"

// NetworkPermissionSet is a 16bit bit-set encoding network-specific permissions.
type NetworkPermissionSet uint16

// NetworkPermissionExtendedSet is a 32bit set encoding network-specific permissions.
//
// This shares values with NetworkPermissionSet.
type NetworkPermissionExtendedSet uint32

// Permissions valid for network operations.
//
// Permissions starting with "accept" cannot fit into the 16bit permission-set
// used by "net_allowed_af" table and cannot be mediated precisely there.
const (
	// NetworkSendPermission indicates that a process may write or send to a socket.
	NetworkSendPermission NetworkPermissionSet = 0x0002 // AA_NET_WRITE or AA_NET_SEND
	// NetworkReceivePermission indicates that a process may read or receive from a socket.
	NetworkReceivePermission      NetworkPermissionSet = 0x0004 // AA_NET_READ or AA_NET_RECEIVE
	NetworkCreatePermission       NetworkPermissionSet = 0x0010 // AA_NET_CREATE
	NetworkShutdownPermission     NetworkPermissionSet = 0x0020 // AA_NET_SHUTDOWN
	NetworkConnectPermission      NetworkPermissionSet = 0x0040 // AA_NET_CONNECT
	NetworkSetAttributePermission NetworkPermissionSet = 0x0100 // AA_NET_SETATTR
	NetworkGetAttributePermission NetworkPermissionSet = 0x0200 // AA_NET_GETATTR

	// Remaining values require 32 bit permission bit-set.

	NetworkAcceptPermission        NetworkPermissionExtendedSet = 0x0010_0000 // AA_NET_ACCEPT
	NetworkBindPermission          NetworkPermissionExtendedSet = 0x0020_0000 // AA_NET_BIND
	NetworkListenPermission        NetworkPermissionExtendedSet = 0x0040_0000 // AA_NET_LISTEN
	NetworkSetOptionPermission     NetworkPermissionExtendedSet = 0x0100_0000 // AA_NET_SETOPT
	NetworkGetOptionPermission     NetworkPermissionExtendedSet = 0x0200_0000 // AA_NET_GETOPT
	NetworkContinueMatchPermission NetworkPermissionExtendedSet = 0x0800_0000 // AA_CONT_MATCH - related to conditional matches.
)

// String returns the human-readable name of the network permission.
func (f NetworkPermissionSet) String() string {
	return networkPermissionSetValues.String(f)
}

// GoString returns the corresponding Go expression.
func (f NetworkPermissionSet) GoString() string {
	return networkPermissionSetValues.GoString(f)
}

// KernelString returns the corresponding Linux kernel expression.
func (f NetworkPermissionSet) KernelString() string {
	return networkPermissionSetValues.KernelString(f)
}

// ParserString returns the corresponding AppArmor parser expression.
func (f NetworkPermissionSet) ParserString() string {
	return networkPermissionSetValues.ParserString(f)
}

// String returns the human-readable name of the extended network permission set.
func (f NetworkPermissionExtendedSet) String() string {
	return networkPermissionExtendedSetValues.String(f)
}

// GoString returns the corresponding Go expression.
func (f NetworkPermissionExtendedSet) GoString() string {
	return networkPermissionExtendedSetValues.GoString(f)
}

// KernelString returns the corresponding Linux kernel expression.
func (f NetworkPermissionExtendedSet) KernelString() string {
	return networkPermissionExtendedSetValues.KernelString(f)
}

// ParserString returns the corresponding AppArmor parser expression.
func (f NetworkPermissionExtendedSet) ParserString() string {
	return networkPermissionExtendedSetValues.ParserString(f)
}

var networkPermissionSetValues = internal.FlagValues[NetworkPermissionSet]{
	{
		Mask:       NetworkSendPermission,
		Name:       "send",
		GoName:     "NetworkSendPermission",
		KernelName: "AA_NET_SEND",
		ParserName: "AA_NET_SEND",
	}, {
		Mask:       NetworkReceivePermission,
		Name:       "receive",
		GoName:     "NetworkReceivePermission",
		KernelName: "AA_NET_RECEIVE",
		ParserName: "AA_NET_RECEIVE",
	}, {
		Mask:       NetworkCreatePermission,
		Name:       "create",
		GoName:     "NetworkCreatePermission",
		KernelName: "AA_NET_CREATE",
		ParserName: "AA_NET_CREATE",
	}, {
		Mask:       NetworkShutdownPermission,
		Name:       "shutdown",
		GoName:     "NetworkShutdownPermission",
		KernelName: "AA_NET_SHUTDOWN",
		ParserName: "AA_NET_SHUTDOWN",
	}, {
		Mask:       NetworkConnectPermission,
		Name:       "connect",
		GoName:     "NetworkConnectPermission",
		KernelName: "AA_NET_CONNECT",
		ParserName: "AA_NET_CONNECT",
	}, {
		Mask:       NetworkSetAttributePermission,
		Name:       "setattr",
		GoName:     "NetworkSetAttributePermission",
		KernelName: "AA_NET_SETATTR",
		ParserName: "AA_NET_SETATTR",
	}, {
		Mask:       NetworkGetAttributePermission,
		Name:       "getattr",
		GoName:     "NetworkGetAttributePermission",
		KernelName: "AA_NET_GETATTR",
		ParserName: "AA_NET_GETATTR",
	},
}

var networkPermissionOnlyExtendedSetValues = internal.FlagValues[NetworkPermissionExtendedSet]{
	{
		Mask:       NetworkAcceptPermission,
		Name:       "accept",
		GoName:     "NetworkAcceptPermission",
		KernelName: "AA_NET_ACCEPT",
		ParserName: "AA_NET_ACCEPT",
	}, {
		Mask:       NetworkBindPermission,
		Name:       "bind",
		GoName:     "NetworkBindPermission",
		KernelName: "AA_NET_BIND",
		ParserName: "AA_NET_BIND",
	}, {
		Mask:       NetworkListenPermission,
		Name:       "listen",
		GoName:     "NetworkListenPermission",
		KernelName: "AA_NET_LISTEN",
		ParserName: "AA_NET_LISTEN",
	}, {
		Mask:       NetworkSetOptionPermission,
		Name:       "setopt",
		GoName:     "NetworkSetOptionPermission",
		KernelName: "AA_NET_SETOPT",
		ParserName: "AA_NET_SETOPT",
	}, {
		Mask:       NetworkGetOptionPermission,
		Name:       "getopt",
		GoName:     "NetworkGetOptionPermission",
		KernelName: "AA_NET_GETOPT",
		ParserName: "AA_NET_GETOPT",
	}, {
		Mask:       NetworkContinueMatchPermission,
		Name:       "cont",
		GoName:     "NetworkContinueMatchPermission",
		KernelName: "AA_CONT_MATCH",
		ParserName: "AA_CONT_MATCH",
	},
}

var networkPermissionExtendedSetValues = make(internal.FlagValues[NetworkPermissionExtendedSet], 0, len(networkPermissionSetValues)+len(networkPermissionOnlyExtendedSetValues))

func init() {
	// Duplicate the lower values of networkPermissionSetValues to networkPermissionExtendedSetValues
	for _, val := range networkPermissionSetValues {
		networkPermissionExtendedSetValues = append(networkPermissionExtendedSetValues, internal.FlagValue[NetworkPermissionExtendedSet]{
			Mask:       NetworkPermissionExtendedSet(val.Mask),
			Name:       val.Name,
			GoName:     val.GoName,
			KernelName: val.KernelName,
			ParserName: val.ParserName,
		})
	}

	networkPermissionExtendedSetValues = append(networkPermissionExtendedSetValues, networkPermissionOnlyExtendedSetValues...)
}
