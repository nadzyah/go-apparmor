// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Canonical Ltd

package apparmor_test

import (
	"errors"
	"io"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"gitlab.com/zygoon/go-apparmor"
)

func TestReadAny(t *testing.T) {
	forEachProfileBinary(t, func(t *testing.T, f *os.File) {
		t.Parallel()

		for {
			off, tag, value, err := apparmor.ReadAny(f)
			if err != nil {
				if errors.Is(err, io.EOF) {
					break
				}

				if value != nil {
					t.Fatalf("%06x %v %v %v", off, tag, value, err)
				} else {
					t.Fatalf("%06x %v - %v", off, tag, err)
				}
			}

			if value != nil {
				t.Logf("%06x %v %v", off, tag, value)
			} else {
				t.Logf("%06x %v", off, tag)
			}
		}
	})
}

func forEachProfileBinary(t *testing.T, fn func(*testing.T, *os.File)) {
	t.Helper()

	ents, err := os.ReadDir("testdata")
	if err != nil {
		t.Fatal(err)
	}

	for _, ent := range ents {
		if !ent.Type().IsRegular() {
			continue
		}

		if !strings.HasSuffix(ent.Name(), ".bin") {
			continue
		}

		t.Run(ent.Name(), func(t *testing.T) {
			f, err := os.Open(filepath.Join("testdata", ent.Name()))
			if err != nil {
				t.Fatal(err)
			}

			t.Cleanup(func() {
				_ = f.Close()
			})

			fn(t, f)
		})
	}
}
